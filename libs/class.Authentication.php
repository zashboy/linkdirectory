<?php
/**
 * 
 */
class Authentication
{
    
    public static function handleLogin()
    {
        Session::init();
        if (!array_key_exists('loggedin', $_SESSION)) {
            Session::destroy();
            header('location: /login?ref='.filter_var(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), FILTER_SANITIZE_URL));
            exit;
        }
    }
    
}

?>