<?php
/**
* class.Controller.php
*
* Base controller class
*
* @category   base controller
* @package    controller
* @author     zashboy
* @copyright  2018 zashboy
* @license    https://www.gnu.org/licenses/gpl-3.0.en.html
* @version    1.0
* @link       https://www.weblink.directory
* @see        controllers folder
* @since      File available since Release 1.0
*/
class Controller
{

    protected $itemsperpage;
    
    public function __construct()
    {
        $this->model = new Model;
        $this->view = new View;
        $this->pagination = new Pagination();

        $this->itemsperpage = ITEMS_PER_PAGE;

    }
    /**
     * getPage()
     * get the requested page data from db by uri
     * @param string $uri
     * @param int $pagenum
     * @return render page
     */
    public function getPage($uri = NULL, $pagenum = NULL)
    {
        //the number of items which we need from db
        $items = ($pagenum > 0) ? ($pagenum - 1) * $this->itemsperpage : $pagenum * $this->itemsperpage;

        try{
            //get the pagedata by url
        	$this->pageData = $this->model->getPage($uri);
            if(!$this->pageData){
                throw new CustomException('404 - Page not found', 404);
            } else {
                //if the page use some sub table get that data
                if($this->pageData[0]['sub_table']){
                    $this->categories = $this->model->getCategories($this->pageData[0]['sub_table'], $items);
                    if(!$this->categories){
                        throw new CustomException('404 - Page not found', 404);
                    } else {

                        //add pagenumber and sql to pagination
                        $pagination = array('pagenum' => $pagenum, 'sql' => $this->categories['sql']);
                        $this->preview($this->pageData, $this->categories['data'], 'list', NULL, $pagination);
                    }
                } else {
                    return $this->view->render($this->pageData, NULL, $this->pageData[0]['name']);
                }

            }
        } catch (CustomException $e) {
            return $e->errorMsg();
        }
    }
    /**
     * getSectionTwo()
     * get the second part of the url request
     * @param string $section_2
     * @param int $pagenum
     */
    public function getSectionTwo($section_2 = NULL, $pagenum = NULL)
    {

        //the number of items which we need from db
        $items = ($pagenum > 0) ? ($pagenum - 1) * $this->itemsperpage : $pagenum * $this->itemsperpage;
        //get the category by url slug name
        $this->category = $this->model->getCategory($section_2);
        //if it has sub category get them
        if($this->category[0]['sub_cat']){
            $this->subCategories = $this->model->getSubCategories($this->category[0]['id'], $items);

            if((!$this->category) || (!$this->subCategories)){
                throw new CustomException('404 - Page not found', 404);
            } else {

                //add pagenumber and sql to pagination
                $pagination = array('pagenum' => $pagenum, 'sql' => $this->subCategories['sql']);
                $this->preview($this->category, $this->subCategories['data'], 'list', NULL, $pagination);

            }
        } else {
            $this->getLinks($this->category[0]['id'], NULL, $pagenum);
        }
    }
    /**
     * getSectionThree()
     * get the third part of the url request from model
     * @param string $section_3
     * @param int $pagenum
     */
    public function getSectionThree($section_3 = NULL, $pagenum = NULL)
    {
        $this->subCategory = $this->model->getSubCategory($section_3);

        $this->getLinks(NULL, $this->subCategory[0]['id'], $pagenum);
    }
    /**
     * getLinks()
     * get the requested links from model
     * @param int $catId
     * @param int $subCatId
     * @param int $pagenum
     * @return caught error
     */
    public function getLinks($catId = NULL, $subCatId = NULL, $pagenum = NULL)
    {
        //the number of items which we need from db
        $items = ($pagenum > 0) ? ($pagenum - 1) * $this->itemsperpage : $pagenum * $this->itemsperpage;

        $this->links = $this->model->getLinks($catId, $subCatId, $items);
        // decide its a category or a subcategory now
        $sourcecat = isset($catId) ? $this->category : (isset($subCatId) ? $this->subCategory : NULL);

        try{
            if(!$this->links){

                throw new CustomException('Empty category', 101);
            } else {
                //add pagenumber and sql to pagination
                $pagination = array('pagenum' => $pagenum, 'sql' => $this->links['sql']);

                $this->preview($sourcecat, $this->links['data'], 'links', NULL, $pagination, TRUE);
            }

        } catch (CustomException $e) {
            return $e->errorMsg();
        }
    }
    /**
     * preview()
     * prepare the page data for the view add pagination to it
     * @param array $metadata
     * @param array $pagedata
     * @param string $tmpl
     * @param array $exc
     * @param array $pagination
     * @param boolean $modals
     * @return rendered page
     */
    public function preview($metadata = NULL, $pagedata = NULL, $tmpl = NULL, $exc = NULL, $pagination = NULL, $modals = NULL)
    {

        $this->pagination->setTotalFromDB($pagination['sql']);
        $this->pagination->setCurrentPage($pagination['pagenum']);
        $pagination['breadcrumbs'] = $this->pagination->showBreadCrumbs();
        $pagination['pages'] = $this->pagination->showPagination();

        return $this->view->render($metadata, $pagedata, $tmpl, $exc, $pagination, $modals);

    }


}// end class

?>