<?php

class Model
{

    /**
     * instanciating db
     */    
    public function __construct()
    {
        $this->db = new Database;
       
    }

    public function setModel($name, $data = NULL)
    {
        $modelname = $name . '_Model';
        if(class_exists($modelname)) {
            if(isset($data)) {
                return new $modelname($data);
            } else {
                return new $modelname;
            }
        }
    }

    /**
     * Get page data from the database
     * @param string $uri 
     * @return array
     */
    public function getPage($uri = NULL)
    {
        $this->uri = $uri;

        $sql = array('reqData' => '*',
                    'tableName' => 'pages',
                    'where' => '`name` = :cond1',
                    'queryArr' => array(':cond1' => $this->uri));

        $this->pageData = $this->db->select($sql);

        if(!$this->pageData){
            return false;

        } else {
            return $this->pageData;
        }
    }
    /**
     * Get all pages data from the database
     * @return array
     */
    public function getPages()
    {
        $sql = array('reqData' => '*',
                    'tableName' => 'pages');

        $this->pageData = $this->db->select($sql);

        if(!$this->pageData){
            return false;

        } else {
            return $this->pageData;
        }
    }
    /**
     * Get categories from the database
     * @param string $sub_table 
     * @return array
     */
    public function getCategories($sub_table = NULL, $items = NULL)
    {
        $limit = isset($items) ? array($items,ITEMS_PER_PAGE) : NULL;

        $sql = array('reqData' => '*',
                    'tableName' => $sub_table,
                    'orderby' => array('title', 'ASC'),
                    'limit' => $limit);

        $this->categories = $this->db->select($sql);
        //set some indexes to null for the pagination
        $sql['limit'] = NULL;
        $sql['orderby'] = NULL;

        if(!$this->categories){
            return false;
        } else {
            return array('data' => $this->categories, 'sql' => $sql);
        }

    }
    /**
     * getCategory()
     * get one category row by slug
     * @param string $section_2
     * @return array or false
     */
    public function getCategory($section_2)
    {

        $sql = array('reqData' => '*',
                    'tableName' => 'categories',
                    'where' => '`slug` = :cond1',
                    'orderby' => NULL,
                    'limit' => NULL,
                    'queryArr' => array(':cond1' => $section_2));

        $this->category = $this->db->select($sql);

        if(!$this->category){
            return false;
        } else {
            return $this->category;
        }

    }
    /**
     * getSubCategories()
     * get all of the sub categories in one category by id
     * @param integer $id
     * @param integer $items
     * @return array or false
     */
    public function getSubCategories($id, $items = NULL)
    {
        $limit = isset($items) ? array($items,ITEMS_PER_PAGE) : NULL;

        $sql = array('reqData' => '*',
                    'tableName' => 'subcategories',
                    'where' => '`catid` = :catid',
                    'orderby' => NULL,
                    'limit' => $limit,
                    'queryArr' => array(':catid' => $id));

        $this->subCategories = $this->db->select($sql);

        $sql['limit'] = NULL;

        if(!$this->subCategories){
            return false;
        } else {
            return array('data' => $this->subCategories, 'sql' => $sql);
        }

    }
    /**
     * getSubCategory()
     * get the row of one subcategory by name (url slug)
     * @param string $section_3
     * @return array or false
     */
    public function getSubCategory($section_3)
    {

        $sql = array('reqData' => '*',
                    'tableName' => 'subcategories',
                    'where' => '`slug` = :cond1',
                    'orderby' => NULL,
                    'limit' => NULL,
                    'queryArr' => array(':cond1' => $section_3));

        $this->subCategory = $this->db->select($sql);

        if(!$this->subCategory){
            return false;
        } else {
            return $this->subCategory;
        }

    }
    /**
     * getLinks()
     * get the links from the db by id
     * @param integer $catId
     * @param integer $subCatId
     * @param integer $items
     * @return array or false
     */
    public function getLinks($catId = NULL, $subCatId = NULL, $items = NULL)
    {
        //if items set than setup the limit to the query eg 3,25
        $limit = isset($items) ? array($items,ITEMS_PER_PAGE) : NULL;
        //set the sql queary array
        $sql = array('reqData' => '*',
                    'tableName' => 'links',
                    'where' => '`catid` = :catid OR `subcatid` = :subcatid',
                    'orderby' => NULL,
                    'limit' => $limit,
                    'queryArr' => array(':catid' => $catId, ':subcatid' => $subCatId));

        //db select
        $this->links = $this->db->select($sql);
        //set the limit index to null because the pagination needs the total number of rows
        $sql['limit'] = NULL;

        if(!$this->links){
            return false;
        } else {
            return array('data' => $this->links, 'sql' => $sql);
        }
    }

}// end class

?>