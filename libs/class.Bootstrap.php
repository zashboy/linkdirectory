<?php
/**
* Bootstrap
*
* Load the necessary files, classes
*
* @category   Bootstrap
* @package    Bootstrap
* @author     zashboy
* @copyright  2018 zashboy
* @license    https://www.gnu.org/licenses/gpl-3.0.en.html
* @version    1.0
* @link       https://www.weblink.directory
* @since      File available since Release 1.0
*/

 class Bootstrap
{

    public function __construct()
    {

        require_once ('config/const.php');

        spl_autoload_register(function($class){
            foreach(glob('*/class.' . $class . '.php') as $config){
                require_once ($config);
            }
            foreach(glob('*/tools/class.' . $class . '.php') as $config){
                if(!class_exists($class, false)){
                    require_once ($config);
                }
            }
            foreach(glob('*/admin/class.' . $class . '.php') as $config){
                require_once ($config);
            }
            foreach(glob('*/mydir/class.' . $class . '.php') as $config){
                require_once ($config);
            }
            if (file_exists( $class . '.php')) {
               require_once $class . '.php';
            }
        });
        
        $this->uri = filter_var(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), FILTER_SANITIZE_URL);
        
        $this->section = explode('/', trim($this->uri, '/'));
        $this->section = array_map('urldecode', $this->section);
        $count = count($this->section);
        
        $this->router = new Router($count, $this->section);
        
    }

}// end class