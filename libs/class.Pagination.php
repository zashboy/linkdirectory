<?php
class Pagination
{

    /** @var int $total number of items */
    protected $total = NULL;

    /** @var string $uri the url of the page */
    protected $uri = NULL;

    /** @var integer $itemsperpage the number of the items per page */
    protected $itemsperpage = NULL;

    /** @var integer $currentpage the number of the current page */
    protected $currentpage = NULL;
    
   public function __construct()
   {

       $this->db = new Database();
       $this->total = 0;
       $this->itemsperpage = ITEMS_PER_PAGE;
       $this->currentpage = 1;
       $this->range = 5;
        //delete the last number from the url
       $this->rawuri = explode('/', trim(filter_var(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), FILTER_SANITIZE_URL), '/'));;
       if(is_numeric(end($this->rawuri))){
        array_pop($this->rawuri);
       } 
       $uri = implode("/", $this->rawuri);
       $this->uri = $uri;
    }

   public function setTotal(int $total = NULL)
   {
       $this->total = $total;
    }

   public function setTotalFromDB(array $sql = NULL)
   {
        //set the sql array request to count
        $sql['reqData'] = 'COUNT(*) AS total';

        if($sql){
            $request = $this->db->select($sql);
            if($request){
                $this->total = $request[0]['total'];
            } else {
                $this->total = NULL;
            }
        }
    }

    public function setUri(string $uri = NULL)
    {
        $this->uri = $uri;
    }

    public function setItemsPerPage(int $itemsperpage = NULL)
    {
        $this->itemsperpage = $itemsperpage;
    }

    public function setCurrentPage(int $currentpage = NULL)
    {
        $this->currentpage = $currentpage;
    }

    public function showPagination()
    {  
        
        $pagestotal = ($this->total > $this->itemsperpage) ? ceil($this->total / $this->itemsperpage) : 1;

        if($this->total > $this->itemsperpage){

            ob_start();
            echo '<div class="row d-flex justify-content-center">';
            echo '<ul class="pagination">';

            //prev
            if ($this->currentpage > 1) {
            echo '<li class="page-item"><a class="page-link" href="/' . $this->uri . '/'. ($this->currentpage - 1) . '">Previous</a></li>';
            }

            if($pagestotal > $this->range){               
                $start = ($this->currentpage <= $this->range) ? 1 : ($this->currentpage - $this->range);
                $end   = ($pagestotal - $this->currentpage >= $this->range) ? ($this->currentpage+$this->range) : $pagestotal;
            }else{
                $start = 1;
                $end   = $pagestotal;
            }   
            //pages
            for ($i = $start; $i <= $end; $i++) {
                $active = (($i==$this->currentpage) || ($this->currentpage == 0 && $i==1)) ? ' active' : NULL; 
                echo '<li class="page-item' . $active . '"><a class="page-link" href="/' . $this->uri . '/'. $i . '">' . $i . '</a></li>';
            }

            //if total pages are more than the current page
            if ($this->currentpage < $pagestotal && $pagestotal != 1) {
                echo '<li class="page-item"><a class="page-link" href="/' . $this->uri . '/'. ($this->currentpage < 1 ? 2 : $this->currentpage + 1) . '">Next</a></li>';
            }
            
            echo '</ul>';
            echo '</div>';

            return ob_get_clean();

        }

    }

    public function showBreadCrumbs()
    {

        ob_start();
        echo '<div class="row">';
        echo '<ul class="breadcrumb">';
        echo '<li class="breadcrumb-item"><a href="/home">Home</a></li>';
        for ($i = 0; $i <= (count($this->rawuri) - 1); $i++){
            if((count($this->rawuri)-1) == $i){
                echo '<li class="breadcrumb-item">'.$this->rawuri[$i].'</li>';
            } else {
                echo '<li class="breadcrumb-item"><a href="/';
                    for($c = 0; $c <= $i; $c++){
                        $slash = ($i == $c) ? NULL : '/';
                        echo $this->rawuri[$c] . $slash ;
                    }
                echo '">'.$this->rawuri[$i].'</a></li>';
            }
        }
        echo '</ul>';
        echo '</div>';
        return ob_get_clean();
    }

}
?>