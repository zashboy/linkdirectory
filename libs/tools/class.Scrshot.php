<?php
class Scrshot
{

    public static function getgpp($url)
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        $googlePagespeedData = file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=$url&screenshot=true", false, stream_context_create($arrContextOptions));

        if(!$googlePagespeedData){
            return NULL;
        } else {

            $googlePagespeedData = json_decode($googlePagespeedData, true);
            $screenshot = $googlePagespeedData['screenshot']['data'];
            $screenshot = str_replace(array('_','-'),array('/','+'),$screenshot);
            $img = "data:image/jpeg;base64,".$screenshot;
            return $img; 
        }
    
    }

    public static function getWss($url = NULL)
    {
       //http://35.204.154.247
        $servurl = "https://webscrshot.appspot.com/json";

        $dataServ = array(
            'url' => $url,
            'post' => true
        );

        $options = array(
            'http' => array (
                'header' => "Content-Type: application/x-www-form-urlencoded",
                'method' => 'POST',
                'content' => http_build_query($dataServ)
            )
        );

        $context  = stream_context_create($options);
        $response = file_get_contents($servurl, false, $context);
         
        if ($response) {
            return json_decode($response);

        } else {
            return NULL;

        }
    }
            
}

?>