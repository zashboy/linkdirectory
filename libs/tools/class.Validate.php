<?php 

class Validate
{
    
    public function __construct() {}
/**
 * checks if the variable wmpty
 * @ return true if its empty, false if not empty
 */
public static function isEmpty($data)
    {
        if (empty($data)) {
            return true;
        } else {
            return false;
        }

    }//end notEmpty()
/**
 * checks only numbers and letters and whitespace in the string
 * @ return false it doesnot match
 */
public static function isOnlyLettersAndNumbers($data)
    {
        // check if name only contains letters, numbers and whitespace
        if (preg_match("/^[a-zA-Z0-9 ]*$/",$data) === 0) {
            return false;
            
        } else {
            return true;
        }
    }//end isOnlyLetters()
/**
 * checks email syntax
 * @ return false it doesnot match
 */
public static function isEmail($data)
    {
        if (filter_var($data, FILTER_VALIDATE_EMAIL)) {
            return true;
            
        } else {
            return false;
        }
    }//end function
/**
 * checks email syntax
 * @ return false it doesnot match
 */
public static function isUrl($data)
    {
        if (filter_var($data, FILTER_VALIDATE_URL)) {
            return true;
            
        } else {
            return false;
        }
    }//end function
/**
 * cleans the iput data
 * @param string or array $data
 * @ return the clean $data
 */
public static function cleanInput($data) {
        
    if(is_array($data)){
        foreach ($data as $key => $value) {
            $data[$key] = trim($value);
            $data[$key] = stripslashes($value);
            $data[$key] = htmlspecialchars($value);
        }
    } else {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
    }
    
    return $data;

    }//end cleanInput
/**
 * check the recapcha
 * @ return false it doesnot match
 */
public static function checkRecaptcha($gRecaptchaResponse = NULL)
    {
    
        $url = 'https://www.google.com/recaptcha/api/siteverify';

        $dataServ = array(
            'secret' => RECAPTCHA_SECRET_KEY,
            'response' => $gRecaptchaResponse
        );

        $options = array(
            'http' => array (
                'header' => "Content-Type: application/x-www-form-urlencoded",
                'method' => 'POST',
                'content' => http_build_query($dataServ)
            )
        );

        $context  = stream_context_create($options);
        $verify = file_get_contents($url, false, $context);
        $captcha_success=json_decode($verify);

        if ($captcha_success->success==false) {
            
            return false;

        } else {

            return true;

        }
            
    }
/**
 * check passwords strength
 * @ return false it doesnot match
 */
public static function checkPsw($psw)
    {
        // Password must be strong
        if(preg_match("/^.*(?=.{8,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$/", $psw) === 0) {
            return false;
        } else {
            return true;
        }
    }
/**
 * Compare passwords
 * @ return false it doesnot match
 */
public static function checkPswsMatch($psw, $repsw)
    {
        if($repsw == $psw){
            return true;
        } else {
            return false;
        }
    }
/**
 * Date mask DD-MM-YYYY
 * @ return false it doesnot match
 */
    public static function checkDate($bday)
    {
        if(preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $bday) === 0) {
            return false;
        } else {
            return true;
        }
    }
/**
 * Checks the date is older than 18
 * @ return false it less than 18
 */
public static function only18($bday)
    {
        if (time() < strtotime('+18 years', strtotime($bday))) {
            return true;
        } else {
            return false;
        }
    }
}//end class


?>