<?php
class AJAXHandler
{
    public function __construct()
    {
        $this->db = new Database;
        $this->view = new View;
    }

    public function saveCategory($data = NULL, $uid = NULL)
    {
        Validate::cleanInput($data);
       // $data['slug'] = URL::url_slug($data['title']);
        $data['userid'] = $uid;
        $data['date'] = date("Y-m-d H:i:s");
        $this->save = $this->db->insertarray('user_categories', $data);
        
        if($this->save){
            
            $this->data = $this->db->select(array('tableName' => 'user_categories', 'where' => array('userid' => $uid)));

            return $this->data;
        }
    }
}

?>