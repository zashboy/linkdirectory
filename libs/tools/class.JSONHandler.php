<?php

class JSONHandler
{
    public function __construct()
    {
        # code...
    }

    public static function read($file = NULL, $arr = true)
    {
        try{
            if(!file_exists($file)){
                throw new Exception("JSON file does not exists");
            } else {
                return json_decode(file_get_contents($file), $arr);
            }
        } catch (Throwable $t) {
            Log::general($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
        }
}

    public static function write($dir = NULL, $contents = NULL)
    {
        $parts = explode('/', $dir);
        $file = array_pop($parts);
        $dir = '';

        foreach($parts as $part) {
            if (! is_dir($dir .= "{$part}/")) mkdir($dir);
        }

        return file_put_contents("{$dir}{$file}", json_encode($contents), FILE_APPEND);

    }

}// end class
