<?php
class View
{

    public function __construct()
    {
        $this->hits = new Hits();

        Session::init();
        if (array_key_exists('loggedin', $_SESSION)) {
            $this->loggedin = true;
        } else {
        	$this->loggedin = false;
        }
    }

    public function setView($name, $data = NULL)
    {
        $viewname = $name . '_View';
        if(class_exists($viewname)) {
            if(isset($data)) {
                return new $viewname($data);
            } else {
                return new $viewname;
            }
        }
    }

    /**
     * render the data
     * @param array $metadata array(array('title' => 'Title'))
     * @param array $data 
     * @param string $body
     * @param array $exc 
     * @param boolean $createmodals TRUE/FALSE
     */
    public function render($metadata = NULL, $data = NULL, $body = 'default', $exc = NULL, $pagination = NULL, $createmodals = NULL)
    {

        $this->hits->save($metadata[0]['title']);

        if (file_exists('tmp/admin/'.TEMPLATE_NAME.'/css/' . $body .'.css')) {
            $css = '<link href="tmp/'.TEMPLATE_NAME.'/css/' . $body .'.css" rel="stylesheet">';
        } else {
            $css = NULL;
        }

        if (file_exists('tmp/'.TEMPLATE_NAME.'/js/' . $body .'.html')) {
            ob_start();
            require_once('tmp/'.TEMPLATE_NAME.'/js/' . $body .'.html');
            $js = ob_get_clean();
        } else {
            $js = NULL;
        }

        if($createmodals){
            $domodals = $this->createmodals($data);
        }

        $alertmsg = NULL;
        //show header
        require_once('tmp/'.TEMPLATE_NAME.'/tmp.header.php');
        //show exceptions
        if(isset($exc)){
            ob_start();
            require_once('tmp/'.TEMPLATE_NAME.'/tmp.alertmsg.php');
            $alertmsg = ob_get_clean();
        }
        //breadcrumbs
        if(isset($pagination['breadcrumbs'])){
            echo $pagination['breadcrumbs'];
        }
        //show body content
        if (file_exists('tmp/'.TEMPLATE_NAME.'/tmp.' . $body .'.php')) {
            require_once('tmp/'.TEMPLATE_NAME.'/tmp.' . $body .'.php');
        } else {
            require_once('tmp/'.TEMPLATE_NAME.'/tmp.default.php');
        }
        //pagination
        if(isset($pagination['pages'])){
            echo $pagination['pages'];
        }
        //show footer
        require_once('tmp/'.TEMPLATE_NAME.'/tmp.footer.php');
    }

    public function renderadmin($metadata = NULL, $data = NULL, $body = 'admindashboard', $exc = NULL)
    {

        $this->hits->save($metadata[0]['title']);

        if (file_exists('tmp/admin/css/' . $body .'.css')) {
            $css = '<link href="tmp/admin/css/' . $body .'.css" rel="stylesheet">';
        } else {
            $css = NULL;
        }

        if (file_exists('tmp/admin/js/' . $body .'.html')) {
            ob_start();
            require_once('tmp/admin/js/' . $body .'.html');
            $js = ob_get_clean();
        } else {
            $js = NULL;
        }

        $alertmsg = NULL;
        require_once('tmp/admin/tmp.header.php');
        if(isset($exc)){
            ob_start();
            require_once('tmp/admin/tmp.alertmsg.php');
            $alertmsg = ob_get_clean();
        }
        require_once('tmp/admin/tmp.' . $body .'.php');
        require_once('tmp/admin/tmp.footer.php');
    }
    
    public function loggedinMenu()
    {
        if($this->loggedin){
            require_once('tmp/'.TEMPLATE_NAME.'/tmp.loggedin.php');
        } else {
            require_once('tmp/'.TEMPLATE_NAME.'/tmp.loggedout.php');
        }
    }

    public function createmodals($data)
    {
        ob_start();
        foreach ($data as $linkdata) {
            $modalarr = [];
            require('tmp/'.TEMPLATE_NAME.'/tmp.modal.php');
            $modalarr[] = ob_get_contents();
        }
        ob_end_clean();
        return $modalarr;
    }
}

?>