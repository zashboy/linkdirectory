<?php

class CustomException extends Exception
{

    public function __construct($message, $code = 0, Exception $previous = null, $usermsgs = NULL)
    {
        parent::__construct($message, $code, $previous);
        $this->view = new View;
        $this->pagination = new Pagination;
        $this->usermsgs = $usermsgs;
    }

    public function setException()
    {
        $options = JSONHandler::read(PATH.'localization/en.exceptions.json');
        if(is_array($this->usermsgs)){
            foreach($this->usermsgs as $errcode){
                $exceptions[] = $options[$errcode];
            }
            return $exceptions;
        } else {
            return array($options[$this->usermsgs]);
        }
    }
    
    public function setError()
    {
        $options = JSONHandler::read(PATH.'localization/en.errorpages.json');
        return $options[$this->getCode()];
    }

    public function errorMsg()
    {
        $pagination['breadcrumbs'] = $this->pagination->showBreadCrumbs();
        echo $this->view->render(array($this->setError()), $this->getMessage(), 'error', NULl, $pagination);
    }


    
}


?>