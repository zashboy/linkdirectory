<?php
use PHPMailer\PHPMailer\PHPMailer;

require_once 'libs/PHPMailer/src/PHPMailer.php';
require_once 'libs/PHPMailer/src/Exception.php';
require_once 'libs/PHPMailer/src/SMTP.php';


class SendMail extends PHPMailer
{

    /**
     * init PHPMailer object
     */
    public function __construct($sender = array('username' => DEFAULT_EMAIL_SENDER_USRNAME,
                                                'password' => DEFAULT_EMAIL_SENDER_PSW,
                                                'senderemail' => DEFAULT_EMAIL_SENDER_EMAIL,
                                                'sendername' => DEFAULT_EMAIL_SENDER_NAME))
    {

        $this->mailer = new PHPMailer(true);
        $this->mailer->isSMTP();
        $this->mailer->SMTPDebug = 0;
        $this->mailer->Host = MAILHOSTNAME;
        $this->mailer->Port = 587;
        $this->mailer->SMTPAuth = true;
        $this->mailer->Username = $sender['username'];
        $this->mailer->Password = $sender['password'];
        $this->mailer->setFrom($sender['senderemail'], $sender['sendername']);
        $this->mailer->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            )
            );    }
    
    /**
     * send mail
     * @param array $from = array('name' => , 'email' =>) 
     * @param array $to = array('name' => , 'email' =>)
     * @param array $cc = array('name' => , 'email' =>) usually the admin's address
     * @param array $msg = array('subject' => , 'body' =>)
     * @param string $html string file name
     * @param string $doc string filename
     * @param string $html_path 'tmp/default/mails/'
     * @param string $doc_html 'tmp/default/mails/doc/'
     */
    public function send($from = NULL, $to = NULL, $cc = NULL, $msg = NULL, $html = NULL, $txt = NULL, $doc = NULL, $html_path = 'tmp/default/mails/', $doc_path = 'tmp/default/mails/doc/')
    {

        try {

            if(isset($from) && isset($to) && isset($msg)) {

                if(isset($html)){
                    ob_start(); //Start output buffering
                    if(file_exists($html_path . $html)){
                        include_once($html_path . $html);
                    } else {
                        include_once('tmp/default/mails/' . $html);
                    }
                    $html_msg = ob_get_clean();
                } else {
                    $html_msg = NULL;
                }

                ob_start(); //Start output buffering
                if(file_exists($html_path . $txt)){
                    str_replace('%name%', $to['name'], include_once($html_path . $txt));
                } else {
                    echo str_replace('%name%', $to['name'], $msg['body']);
                }
                $text_msg = ob_get_clean();

                //copy to someone
                if(isset($cc)){

                    $this->mailer->addReplyTo($to['email'], $to['name']);
                    $this->mailer->addAddress($cc['email'], $cc['name']);
                    $this->mailer->Subject = $msg['subject'];
                    $this->mailer->msgHTML($html_msg);
                    $this->mailer->AltBody = $text_msg;
                    if(isset($doc)) {
                        $this->mailer->addAttachment($doc_path . $doc);
                    }
                    $this->mailer->send();
                }
                
                $this->mailer->addReplyTo($from['email'], $from['name']);
                $this->mailer->addAddress($to['email'], $to['name']);
                $this->mailer->Subject = $msg['subject'];

                if(isset($html)){

                    $this->mailer->msgHTML($html_msg);
                    $this->mailer->AltBody = $text_msg;
                    if(isset($doc)) {
                        $this->mailer->addAttachment($doc_path . $doc);
                    }
                    $this->mailer->send();
                } else {

                    $this->mailer->addReplyTo($from['email'], $from['name']);
                    $this->mailer->addAddress($to['email'], $to['name']);
                    $this->mailer->Subject = $msg['subject'];
                    $this->mailer->msgHTML($html_msg);
                    $this->mailer->AltBody = $text_msg;
                    if(isset($doc)) {
                        $this->mailer->addAttachment($doc_path . $doc);
                    }
                    $this->mailer->send();
                }
                return true;
            }

        } catch(Exception $e){
            log::user('PHPMailer contact us mail failed. name: '.$to['name'].' email: '.$to['email'].' errmsg: '.$e->errorMessage());
            return false;
        }

    }// end sendmail()


}// end class



?>