<?php

class Log {
  //

  /*
   User Errors...
  */
    public static function user($msg)
    {

    $date = date("Y-m-d H:i:s");
    $log = $msg."   |  Date:  ".$date."\n";
    error_log($log, 3, USER_ERROR_DIR);
    
    }
    /*
   General Errors...
  */
    public static function general($msg)
    {

    $date = date("Y-m-d H:i:s");
    $log = $msg."   |  Date:  ".$date."\n";
    error_log($log, 3, GENERAL_ERROR_DIR);

    }

    public static function searchlog($msg)
    {

    $date = date("Y-m-d H:i:s");
    $log = $msg."   |  Date:  ".$date."\n";
    error_log($log, 3, SEARCH_QUERY_DIR);

    }
    
}//end log class

?>