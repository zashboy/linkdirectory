<?php
/**
* class.Database
*
* PDO database handler class
*
* @category   database wrapper
* @package    library
* @author     zashboy
* @copyright  2018 zashboy
* @license    https://www.gnu.org/licenses/gpl-3.0.en.html
* @version    1.0
* @link       https://www.weblink.directory
* @see        PDO
* @since      File available since Release 1.0
*/
class Database extends PDO
{

    public function __construct()
    {

        parent::__construct("mysql:host=" . HOSTNAME . ";dbname=" . DBNAME, USERNAME, PASSWORD);

    }

    /**
     * Get result set from the database 
     * @param array $array index options    string reqData => '*' (default value '*')
     *                                      string tableName => 'users'
     *                                      string or array where => '`id`' = :id or array('id' => $id)
     *                                      array or string orderby => array('title', 'ASC')
     *                                      int or array limit => array(2,30)
     *                                      array queryArr => array(':id' => $id)
     * @return array
     */
    public function select($array = NULL)
    {

        $reqData = $tableName = $where = $orderby = $limit = $queryArr = NULL;

        if(is_array($array)){
            foreach ($array as $key => $value) {
                $$key = $value;
            }
        }

        $reqData = isset($reqData) ? $reqData : '*';

        if(isset($where)){

            if(is_array($where) && count($where) > 0){

                $_where .= 'WHERE ';

                foreach ($where as $key => $value) {
                    $_where .= '`' . $key . '` = :' . $key;

                        for ($i=1; $i < count($where); $i++) { 
                            $_where .= ', ';
                        }
                    $_where .= ' ';
                    //prepare the $queryArr if it hasnt been set    
                    if(!isset($queryArr)){
                        $queryArr[':' . $key] = $value;
                    }
                }
                $where = $_where;

            } else {
                $where = 'WHERE ' . $where . ' ';
            }
        } else {
            $where = NULL;
        }
        $orderby = isset($orderby) ? (is_array($orderby) ? 'ORDER BY `' . $orderby[0] . '` ' . $orderby[1] . ' ' : 'ORDER BY `' . $orderby . '` ') : NULL;
        $limit = isset($limit) ? (is_array($limit) ? 'LIMIT ' . $limit[0] . ',' . $limit[1] . ' ' : 'LIMIT ' . $limit . ' ') : NULL;
        
        try {
            if($reqData && $tableName){
                $this->query = $this->prepare("SELECT " . $reqData  . 
                                                " FROM `" . $tableName . "` " . 
                                                    $where .  $orderby .  $limit);
                $this->dataRaw = $this->query->execute($queryArr);
                if($this->query->rowCount() != 0){
                        $this->data = $this->query->fetchAll(PDO::FETCH_ASSOC);
                        return $this->data;
                    } else {
                        return NULL;
                    }  
            } else {
                return NULL;
            }
        } catch (Throwable $t) {
            Log::general($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
        }

    }
    
    /**
     * Update data in the database
     * @param string $tableName
     * @param string $condition '`name` = :uri WHERE `id` = :id'
     * @param array $queryArr array(':uri' => $this->uri)
     * @return array
     */
    public function update($tableName = NULL, $condition = NULL, $queryArr = NULL)
    {
        if($tableName && $condition){
            try{ 
                $this->query = $this->prepare("UPDATE `".$tableName."` SET ".$condition);
                $this->dataRaw = $this->query->execute($queryArr);
                    if($this->dataRaw && $this->query->rowCount() != 0){
                        return TRUE;
                    } else {
                        return FALSE;
                    }  
            } catch (Throwable $t) {
                Log::general($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            }
        } else {
            return NULL;
        }
    }

    /**
     * Insert into database
     * @param string $tableName
     * @param string $col col,col,col
     * @param string $value :val1, :v al2, :val3
     * @param array $queryArr array(':uri' => $this->uri)
     * @return boolean
     */
    public function insert($tableName = NULL, $col = NULL, $value = NULL, $queryArr = NULL)
    {
        if($tableName && $col && $value){
            try{ 
                $this->query = $this->prepare("INSERT INTO `".$tableName."`(".$col.") VALUES (".$value.")");
                $this->dataRaw = $this->query->execute($queryArr);
                    if($this->dataRaw && $this->query->rowCount() != 0){
                        return TRUE;
                    } else {
                        return FALSE;
                    }  
            } catch (Throwable $t) {
                Log::general($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            }
        } else {
            return NULL;
        }
    }
    /**
     * Insert an array into database
     * @param string $tableName
     * @param array $array($col => $value)
     * @return boolean
     */
    public function insertarray($tableName = NULL, $array = NULL)
    {
        if($tableName && $array){

            $col = implode(',', array_keys($array));
            $value = ':' . implode(',:', array_keys($array));
            $queryArr = array_combine(explode(',', $value), $array);

            try{ 
                $this->query = $this->prepare("INSERT INTO `".$tableName."`(".$col.") VALUES (".$value.")");
                $this->dataRaw = $this->query->execute($queryArr);
                if($this->dataRaw && $this->query->rowCount() != 0){
                        return TRUE;
                    } else {
                        return FALSE;
                    }  
            } catch (Throwable $t) {
                Log::general($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            }
        } else {
            return NULL;
        }
    }

    /**
     * DELETE from database
     * @param string $tableName
     * @param string $tableName
     * @param string $condition 'WHERE `name` = :uri'
     * @param array $queryArr array(':uri' => $this->uri)
     * @return boolean
     */
    public function delete($tableName = NULL, $condition = NULL, $queryArr = NULL)
    {
        if($tableName && $condition){
            try{ 
                $this->query = $this->prepare("DELETE FROM `".$tableName."` ".$condition);
                $this->dataRaw = $this->query->execute($queryArr);
                    if($this->dataRaw && $this->query->rowCount() != 0){
                        return TRUE;
                    } else {
                        return FALSE;
                    }  
            } catch (Throwable $t) {
                Log::general($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            }
        } else {
            return NULL;
        }
    }

}// end class