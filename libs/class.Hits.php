<?php
class Hits
{
    public function __construct()
    {
        $this->db = new Database;

    }

    public function save($title = NULL)
    {

        $this->db->insert('hits', 'uri,title,http_referer,http_user_agent,remote_address,raw_data,date',
        ':uri,:title,:http_referer,:http_user_agent,:remote_address,:raw_data,:date',
        array(':uri' => $_SERVER['REQUEST_URI'],
        ':title' => $title,  
        ':http_referer' => isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:NULL,
        ':http_user_agent' => isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:NULL,
        ':remote_address' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
        ':raw_data' => json_encode($_SERVER),
        ':date' => date("Y-m-d H:i:s")));
    }
}
?>