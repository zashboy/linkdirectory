<?php
class activation_Model extends Model
{

    public function __construct($token)
    {

        parent::__construct();

        $this->token = $token;

        try{

            $sql = array('reqData' => '`activated`',
                        'tableName' => 'users',
                        'where' => '`token` = :token',
                        'orderby' => NULL,
                        'limit' => NULL,
                        'queryArr' => array(':token' => $this->token));


            $this->actCheck1 = $this->db->select($sql);
            
            if($this->actCheck1[0]['activated'] != 1) {
                $this->actCheck2 = $this->db->update('users', '`activated` = 1 WHERE `token` = :token', array(':token' => $this->token));
            } else {
                throw new CustomException('This account has been activated already', 103);
            }

        } catch(CustomException $e) {
            throw $e;

        } catch(Throwable $t) {
            Log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
        } 
    }

    public function result()
    {
        try{
            if($this->actCheck2) {
                return TRUE;
            } else {
                throw new Exception('Could not update user activation: '. $this->token);
            }
        } catch(Throwable $t) {
            Log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            return false;
        }
    }
    
}

?>