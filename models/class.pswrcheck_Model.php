<?php
/**
* class.pswrcheck_Model
*
* Checks the token and save the new password on password reset
*
* @category   Class
* @package    Model
* @author     zashboy
* @copyright  2018 zashboy
* @license    https://www.gnu.org/licenses/gpl-3.0.en.html
* @version    1.0
* @link       https://www.weblink.directory
* @since      File available since Release 1.0
*/
class pswrcheck_Model extends Model
{

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Checks the token in the database if its valid then
     * it checks it is not older then 24 hours and
     * loads the users data to the session variable
     * 
     * @param string $token password reset token sended by email
     * @return true or throw exception
     */
    public function checktoken($token)
    {
        try{

            $sql = array('reqData' => '*',
                        'tableName' => 'users',
                        'where' => '`pswrtoken` = :pswrtoken',
                        'queryArr' => array(':pswrtoken' => $token['token']));
    
            $result = $this->db->select($sql);

            if($result){
                //if now() smaller then request time + 1 day means its in 24 hours range
                if(time() < strtotime('+1 day', strtotime($result[0]['pswrtime']))){
                    Session::init();
                    foreach ($result as $arr) {
                        foreach ($arr as $key => $value) {
                            Session::set($key, $value);
                        }
                    }
                    Session::set('token', $token['token']);
                    return TRUE;
                } else {
                    throw new CustomException(NULL, 0, NULL, 1037 );
                }
            } else {
                throw new CustomException(NULL, 0, NULL, 1034);
            }
        } catch(CustomException $e) {
            throw $e;
        } catch(Throwable $t) {
            log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            return FALSE;
        }
    
    }
    /**
     *It saves the new password to the database and deletes the 
     *recovery token, send mail to the user about the changed password
     *  
     * @param string $data the new password
     * @return true or log an exception
     */
    public function savepsw($data)
    {
        try {
            Session::init();
            //delete the token, update the field to null
            $this->insert = $this->db->update('users',
            '`psw` = :psw, `pswrtoken` = NULL, `date` = :date WHERE `id` = :id',
            array(':psw' => password_hash($data['psw'], PASSWORD_BCRYPT),
                  ':date' => date("Y-m-d H:i:s"),
                  ':id' => Session::get('id')));

            $this->sendmail = new SendMail;
            $this->sendmail->send($from = array('name' => WEBSITE_NAME,
                                                'email' => WEBSITE_REPLYTO_EMAIL),
                                $to = array('name' => Session::get('username'),
                                'email' => Session::get('email'),
                                'token' => Session::get('token')),
                                $cc = array('name' => 'Admin',
                                'email' => 'zashboy@gmail.com'),
                                $msg = array('subject' => 'Your password has been changed on ' . WEBSITE_NAME,
                                'body' => 'Dear %name%, Your password has been changed, read the html message.'),
                                $html = 'psw-changed.php',
                                $txt = 'psw-changed.txt',
                                $doc = NULL,
                                $html_path = 'tmp/'.TEMPLATE_NAME.'/mails/');
                                return TRUE;

            } catch(Throwable $t) {
                log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
                return FALSE;
            }
        }

}//endclass

?>