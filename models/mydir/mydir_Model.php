<?php

class mydir_Model extends Model
{

    private $input;

    public function __construct($data = NULL)
    {
        parent::__construct();
        $this->input = $data;

        try{

            $sql = array('tableName' => 'user_categories',
                        'where' => array('id' => $_SESSION['userid']));

            $this->usercategories = $this->db->select($sql);

            $result = $this->db->select($sql);
            
            if($result){
                return $result;
            } else {
                return FALSE;
            }
            
        } catch(Throwable $t) {
            log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            return FALSE;
        }
    
    }

    public function result()
    {
        if(Session::get('loggedin') == $this->input['username']){
            return TRUE;
        } else {
            return FALSE;
        }
    }

}//endclass

?>