<?php

class login_Model extends Model
{

    private $input;

    public function __construct($data)
    {
        parent::__construct();
        $this->input = $data;

        try{

            $sql = array('reqData' => '*',
                        'tableName' => 'users',
                        'where' => '`username` = :inputname',
                        'queryArr' => array(':inputname' => $this->input['username']));
    
            $result = $this->db->select($sql);
            if($result){
                if(password_verify($this->input['psw'], $result[0]['psw'])){
                    if($result[0]['activated'] == 1) {
                        Session::set('loggedin', $result[0]['username']);
                        Session::set('userid', $result[0]['id']);
                    } else {
                        throw new CustomException(NULL, 0, NULL, 1013);
                    }
                } else {
                    throw new CustomException(NULL, 0, NULL, 1014);
                }
            } else {
                throw new CustomException(NULL, 0, NULL, 1015);
            }
        } catch(CustomException $e) {
            throw $e;
        } catch(Throwable $t) {
            log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            return FALSE;
        }
    
    }

    public function result()
    {
        if(Session::get('loggedin') == $this->input['username']){
            return TRUE;
        } else {
            return FALSE;
        }
    }

}//endclass

?>