<?php

class addlink_Model extends Model
{

    private $input;

    public function __construct($data)
    {
        parent::__construct();
        $this->input = $data;

        try{

            $sql = array('reqData' => '*',
                        'tableName' => 'links',
                        'where' => '`link_url` = :input_link_url',
                        'queryArr' => array(':input_link_url' => $this->input['link_url']));
            //check if the link is in the db already
            $result = $this->db->select($sql);
            //if not then save
            if(!$result){
                $bindarr = [];
                $insto = '';
                $insval = '';
                foreach($this->input as $key => $value){
                    $insto .= '`' . $key . '`, ';
                    $insval .= ':' . $key . ', ';
                    $bindarr[':' . $key] = $value;
                }
                $this->insert = $this->db->insert('links', trim($insto, ', '), trim($insval, ', '), $bindarr);
                /*$this->sendmail = new SendMail;
                $this->sendmail->send($from = array('name' => WEBSITE_NAME,
                                                    'email' => WEBSITE_REPLYTO_EMAIL),
                                    $to = array('name' => $this->input['username'],
                                    'email' => $this->input['email'],
                                    'token' => $userToken),
                                    $cc = array('name' => 'Admin',
                                    'email' => 'zashboy@gmail.com'),
                                    $msg = array('subject' => 'Activation email from Weblink directory',
                                    'body' => 'Dear %name%, To activate your account you need to read the html message.'),
                                    $html = 'activate-acc-mail.php');*/

            } else {

                foreach($result as $arr) {
                    if(in_array($this->input['link_url'], $arr)){
                        throw new CustomException(NULL, 0, NULL, 1021);
                    }
                }
            }
        } catch(CustomException $e) {
            throw $e;
        } catch(Throwable $t) {
            Log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            return false;
        }
    
    }

    public function result()
    {
        try{
            if($this->insert) {
                return TRUE;
            } else {
                throw new Exception('Could not save user data: '. json_encode($this->input));
            }
        } catch(Throwable $t) {
            Log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            return false;
        }

    }

}//endclass

?>