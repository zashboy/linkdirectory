<?php

class passwordreset_Model extends Model
{

    private $input;

    public function __construct($data)
    {
        parent::__construct();
        $this->input = $data;

        try{

            $sql = array('reqData' => '*',
                        'tableName' => 'users',
                        'where' => '`email` = :email',
                        'queryArr' => array(':email' => $this->input['email']));
    
            $result = $this->db->select($sql);
            if($result){
                $token = md5($this->input['email'].random_bytes(12).time());
                $this->insert = $this->db->update('users',
                 '`pswrtoken` = :pswrtoken, `pswrtime` = :pswrtime WHERE `id` = :id',
                   array(':pswrtoken' => $token,
                         'pswrtime' => date("Y-m-d H:i:s"),
                         'id' => $result[0]['id']));
                $this->sendmail = new SendMail;
                $this->sendmail->send($from = array('name' => WEBSITE_NAME,
                                                    'email' => WEBSITE_REPLYTO_EMAIL),
                                    $to = array('name' => $result[0]['username'],
                                    'email' => $this->input['email'],
                                    'token' => $token),
                                    $cc = array('name' => 'Admin',
                                    'email' => 'zashboy@gmail.com'),
                                    $msg = array('subject' => 'Password recovery from ' . WEBSITE_NAME,
                                    'body' => 'Dear %name%, To recover your account you need to read the html message.'),
                                    $html = 'pswr-mail.php',
                                    $txt = 'pswr-mail.txt',
                                    $doc = NULL,
                                    $html_path = 'tmp/'.TEMPLATE_NAME.'/mails/');
            } else {
                throw new CustomException(NULL, 0, NULL, 1032);
            }
        } catch(CustomException $e) {
            throw $e;
        } catch(Throwable $t) {
            log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            return FALSE;
        }
    
    }

    public function result()
    { 
        try{
            if($this->insert){
                return TRUE;
            } else {
                throw new Exception('Could not save password reset token: '. json_encode($this->input));
            }
        } catch(Throwable $t) {
            Log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            return false;
        }
    }

}//endclass

?>