<?php

class adminlogin_Model extends Model
{

    private $input;

    public function __construct($data)
    {
        parent::__construct();
        $this->input = $data;

        try{

            $result = $this->db->select('*', 'users', '`username` = :inputname', NULL, NULL, array(':inputname' => $this->input['username']));
            if($result){
                if(password_verify($this->input['psw'], $result[0]['psw'])){
                    if($result[0]['activated'] == 1) {
                        if($result[0]['perm'] == 1) {
                            Session::set('username', $result[0]['username']);
                        } else {
                            throw new CustomException(NULL, 0, NULL, 1024);
                        }
                    } else {
                        throw new CustomException(NULL, 0, NULL, 1013);
                    }
                } else {
                    throw new CustomException(NULL, 0, NULL, 1014);
                }
            } else {
                throw new CustomException(NULL, 0, NULL, 1015);
            }
        } catch(CustomException $e) {
            throw $e;
        } catch(Throwable $t) {
            log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            return FALSE;
        }
    
    }

    public function result()
    {
        if(Session::get('username') == $this->input['username']){
            return TRUE;
        } else {
            return FALSE;
        }
    }

}//endclass

?>