<?php

class admin_newpage_Model extends Model
{

    private $input;

    public function __construct($data)
    {
        parent::__construct();
        $this->input = $data;

        try{

            $result = $this->db->select('name', 'pages', '`name` = :inputname', NULL, NULL,
            array(':inputname' => $this->input['name']));
            
            if(!$result){
                $this->insert = $this->db->insertarray('pages', $this->input);

            } else {
                throw new CustomException(NULL, 0, NULL, 1025);

            }
        } catch(CustomException $e) {
            throw $e;
        } catch(Throwable $t) {
            Log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            return false;
        }
    
    }

    public function result()
    {
        try{
            if($this->insert) {
                return TRUE;
            } else {
                throw new Exception('Could not save user data: '. json_encode($this->input));
            }
        } catch(Throwable $t) {
            Log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            return false;
        }

    }

}//endclass

?>