<?php

class signup_Model extends Model
{

    private $input;

    public function __construct($data)
    {
        parent::__construct();
        $this->input = $data;

        try{

            $sql = array('reqData' => '*',
                        'tableName' => 'users',
                        'where' => '`username` = :inputname OR `email` = :inputemail',
                        'queryArr' => array(':inputname' => $this->input['username'],
                                            ':inputemail' => $this->input['email']));

            $result = $this->db->select($sql);

            if(!$result){
                $userToken = md5($this->input['email'].time());
                $this->insert = $this->db->insert('users',
                 '`username`, `psw`, `email`, `bday`, `token`, `date`',
                  ':nname, :psw, :email, :bday, :token, :ndate',
                   array(':nname' => $this->input['username'],
                        ':psw' => password_hash($this->input['psw'], PASSWORD_BCRYPT),
                        ':email' => $this->input['email'],
                        ':bday' => $this->input['bday'],
                        ':token' => $userToken,
                        ':ndate' => date("Y-m-d H:i:s")));
                $this->sendmail = new SendMail;
                $this->sendmail->send($from = array('name' => WEBSITE_NAME,
                                                    'email' => WEBSITE_REPLYTO_EMAIL),
                                    $to = array('name' => $this->input['username'],
                                    'email' => $this->input['email'],
                                    'token' => $userToken),
                                    $cc = array('name' => 'Admin',
                                    'email' => 'zashboy@gmail.com'),
                                    $msg = array('subject' => 'Activation email from Weblink directory',
                                    'body' => 'Dear %name%, To activate your account you need to read the html message.'),
                                    $html = 'activate-acc-mail.php',
                                    NULL,
                                    NULL,
                                    $html_path = 'tmp/'. TEMPLATE_NAME . '/mails/');

            } else {

                foreach($result as $arr) {
                    if(in_array($this->input['username'], $arr)){
                        throw new CustomException(NULL, 0, NULL, 1009);
                    }
                }
                foreach($result as $arr){ 
                    if (in_array($this->input['email'], $arr)) {
                        throw new CustomException(NULL, 0, NULL, 1010);
                    }
                }
            }
        } catch(CustomException $e) {
            throw $e;
        } catch(Throwable $t) {
            Log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            return false;
        }
    
    }

    public function result()
    {
        try{
            if($this->insert) {
                return TRUE;
            } else {
                throw new Exception('Could not save user data: '. json_encode($this->input));
            }
        } catch(Throwable $t) {
            Log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            return false;
        }

    }

}//endclass

?>