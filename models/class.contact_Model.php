<?php

class contact_Model extends Model
{

    private $input;

    public function __construct($data)
    {
        parent::__construct();
        $this->input = $data;

        try{

            $this->insert = $this->db->insert('messages',
                '`name`, `email`, `msg`, `date`',
                ':nname, :email, :msg, :ndate',
                array(':nname' => $this->input['name'],
                    ':email' => $this->input['email'],
                    ':msg' => $this->input['msg'],
                    ':ndate' => date("Y-m-d H:i:s")));
            $this->sendmail = new SendMail;
            $this->sendmail->send($from = array('name' => WEBSITE_NAME,
                                                'email' => WEBSITE_REPLYTO_EMAIL),
                                $to = array('name' => $this->input['name'],
                                'email' => $this->input['email'],
                                'token' => $userToken),
                                $cc = array('name' => 'Admin',
                                'email' => 'zashboy@gmail.com'),
                                $msg = array('subject' => 'You have sent a message to' . WEBSITE_NAME,
                                'body' => 'Dear %name%, Please read the html message.'),
                                $html = 'contact-us-mail.php',
                                NULL,
                                NULL,
                                $html_path = 'tmp/' . TEMPLATE_NAME . '/mails/');

        } catch(Throwable $t) {
            Log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            return false;
        }
    
    }

    public function result()
    {
        try{
            if($this->insert) {
                return TRUE;
            } else {
                throw new Exception('Could not save user data: '. json_encode($this->input));
            }
        } catch(Throwable $t) {
            Log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            return false;
        }

    }

}//endclass

?>