<?php

class login_Controller extends Controller
{

    
   public function __construct()
   {
        parent::__construct();

            try {
                
                if(array_key_exists('submit', $_POST)) {

                    if(Validate::isEmpty($_POST['name'])) {
                        throw new CustomException(NULL, 0, NULL, 1001);
                    } elseif (Validate::isEmpty($_POST['psw'])) {
                        throw new CustomException(NULL, 0, NULL, 1002);
                    }
                    $this->data = array('username' => $_POST['name'], 'psw' => $_POST['psw']);
                    $this->login = $this->model->setModel('login', $this->data);
                    //get the saved request_query variable from the hidden field if it setted 
                    $referer = !empty($_POST['ref']) ? $_POST['ref'] : '/mydir';

                    if($this->login->result()){
                        header('Location: ' . $referer);
                    } else {
                        $this->view->render(NULL, NULL, 'login');
                    }
                } else {
                    $this->view->render(NULL, NULL, 'login');
                }

            } catch (CustomException $e){
                return $this->view->render(NULL, NULL, 'login', $e->setException());
            } catch(Throwable $t){
                log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            }
   }
}

?>