<?php
/**
* class.passwordeset_Controller.php
*
* It gets the $_POST['email'] from the form and pass it
* to the model
*
* @category   Password reset controller
* @package    Controller
* @author     zashboy
* @copyright  2018 zashboy
* @license    https://www.gnu.org/licenses/gpl-3.0.en.html
* @version    1.0
* @link       https://www.weblink.directory
* @see        passwordreset_Model(), pswrcheck model and controller
* @since      File available since Release 1.0
*/
class passwordreset_Controller extends Controller
{

    /**
     * gets $_POST['email'] and pass it to model after validation
     * @param string $_POST['email']
     * @return throw exception on fail and pass
     */    
   public function __construct()
   {
        parent::__construct();

            try {
               
                if(array_key_exists('submit', $_POST)) {

                    if(Validate::isEmpty($_POST['email'])) {
                        throw new CustomException(NULL, 0, NULL, 1005);
                    } elseif (!Validate::isEmail($_POST['email'])) {
                        throw new CustomException(NULL, 0, NULL, 1005);
                    }elseif(!Validate::checkRecaptcha($_POST['g-recaptcha-response'])){
                        throw new CustomException(NULL, 0, NULL, 1029);
                    }
                    $this->data = array('email' => $_POST['email']);
                    $this->passwordreset = $this->model->setModel('passwordreset', $this->data);

                    if($this->passwordreset->result()){
                        $this->view->render(NULL, NULL, 'passwordreset', (new CustomException(NULL, 0, NULL, 1033))->setException()); 
                    } else {
                        $this->view->render(NULL, NULL, 'passwordreset');
                    }
                } else {
                    $this->view->render(NULL, NULL, 'passwordreset');
                }

            } catch (CustomException $e){
                return $this->view->render(NULL, NULL, 'passwordreset', $e->setException());
            } catch(Throwable $t){
                log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            }
   }
}

?>