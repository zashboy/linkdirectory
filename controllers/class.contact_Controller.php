<?php

class contact_Controller extends Controller
{

    
   public function __construct()
   {
        parent::__construct();

            try {
               
                if(array_key_exists('submit', $_POST)) {

                    foreach($_POST as $key => $value){
                        Validate::cleanInput($value);
                    }

                    $this->usermsgs = array();
                    if(Validate::isEmpty($_POST['name'])) {
                        $this->usermsgs[] = 1027;
                    }
                    if(Validate::isEmpty($_POST['email'])) {
                        $this->usermsgs[] = 1005;
                    } 
                    if(Validate::isEmpty($_POST['message'])) {
                        $this->usermsgs[] = 1028;
                    }
                    if(!Validate::isOnlyLettersAndNumbers($_POST['name'])){
                        $this->usermsgs[] = 1004;
                    }
                    if(!Validate::isEmail($_POST['email'])){
                        $this->usermsgs[] = 1005;
                    }
                    if(!Validate::checkRecaptcha($_POST['g-recaptcha-response'])){
                        $this->usermsgs[] = 1029;
                    }
                    $count = count($this->usermsgs);

                    if($count > 0){
                        throw new CustomException(NULL, 0, NULL, $this->usermsgs);
                    }
                    $this->data = array('name' => $_POST['name'],
                                        'email' => $_POST['email'],
                                        'msg' => $_POST['message'],
                                        'date' => date("Y-m-d H:i:s"));
                                        
                   $this->contact = $this->model->setModel('contact', $this->data);

                   if($this->contact->result()) {
	                    $this->view->render(NULL, NULL, 'contact', (new CustomException(NULL, 0, NULL, 1030))->setException());
                   } else {
                       throw new CustomException(NULL, 0, NULL, 1031);
                   }
                    
                } else {
                    $this->view->render(NULL, NULL, 'contact');
                }

            } catch (CustomException $e){
                return $this->view->render(NULL, NULL, 'contact', $e->setException());
            } catch(Throwable $t){
                Log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            }
   }
}

?>