<?php
class logout_Controller extends Controller
{
    public function __construct()
    {
        parent::__construct();
        Session::destroy();
        header('Location: /login');
    }
}

?>