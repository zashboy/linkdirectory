<?php 

class activation_Controller extends Controller
{
    public function __construct()
    {

        parent::__construct();
        
        try {
            if(array_key_exists('token', $_GET)){

                $this->token = Validate::cleanInput($_GET['token']);
                $this->activation = $this->model->setModel('activation', $this->token);

                if($this->activation->result()){
                    $this->view->render(NULL, NULL, 'login', (new CustomException(NULL, 0, NULL, 1012))->setException());
                }
            } else {
                throw new CustomException('Something went wrong', 102);
            }
        } catch (CustomException $e) {
            return $e->errorMsg();
        }
    }
}

?>