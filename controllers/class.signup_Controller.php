<?php

class signup_Controller extends Controller
{

    
   public function __construct()
   {
        parent::__construct();

            try {
               
                if(array_key_exists('submit', $_POST)) {

                    foreach($_POST as $key => $value){
                        Validate::cleanInput($value);
                    }

                    $this->usermsgs = array();
                    if(Validate::isEmpty($_POST['name'])) {
                        $this->usermsgs[] = 1001;
                    }
                    if(Validate::isEmpty($_POST['psw'])) {
                        $this->usermsgs[] = 1002;
                    } 
                    if(Validate::isEmpty($_POST['repsw'])) {
                        $this->usermsgs[] = 1003;
                    }
                    if(!Validate::isOnlyLettersAndNumbers($_POST['name'])){
                        $this->usermsgs[] = 1004;
                    }
                    if(!Validate::isEmail($_POST['email'])){
                        $this->usermsgs[] = 1005;
                    }
                    if(!Validate::checkPsw($_POST['psw'])){
                        $this->usermsgs[] = 1006;
                    }
                    if(!Validate::checkPswsMatch($_POST['psw'], $_POST['repsw'])){
                        $this->usermsgs[] = 1007;
                    }
                    if(!Validate::checkRecaptcha($_POST['g-recaptcha-response'])){
                        $this->usermsgs[] = 1029;
                    }

                    // comment it out if the age important
                    /*if(!Validate::checkDate($_POST['bday'])){
                        $this->usermsgs[] = 1008;
                    }*/
                    $count = count($this->usermsgs);

                    if($count > 0){
                        throw new CustomException(NULL, 0, NULL, $this->usermsgs);
                    }
                    $this->data = array('username' => $_POST['name'],
                                        'email' => $_POST['email'],
                                        'psw' => $_POST['psw'],
                                        'repsw' => $_POST['repsw'],
                                        'bday' => $_POST['bday']);
                                        
                   $this->signup = $this->model->setModel('signup', $this->data);

                   if($this->signup->result()) {
	                    $this->view->render(NULL, NULL, 'login', (new CustomException(NULL, 0, NULL, 1011))->setException()); //some normal landing page after login
                   }
                    
                } else {
                    $this->view->render(NULL, NULL, 'signup');
                }

            } catch (CustomException $e){
                return $this->view->render(NULL, NULL, 'signup', $e->setException());
            } catch(Throwable $t){
                Log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            }
   }
}

?>