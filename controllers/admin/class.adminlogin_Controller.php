<?php

class adminlogin_Controller extends Controller
{

    
   public function __construct()
   {
        parent::__construct();

            try {
               
                if(array_key_exists('submit', $_POST)) {

                    if(Validate::isEmpty($_POST['name'])) {
                        throw new CustomException(NULL, 0, NULL, 1001);
                    } elseif (Validate::isEmpty($_POST['psw'])) {
                        throw new CustomException(NULL, 0, NULL, 1002);
                    }
                    $this->data = array('username' => $_POST['name'], 'psw' => $_POST['psw']);
                    $this->login = $this->model->setModel('adminlogin', $this->data);

                    if($this->login->result()){
                        header('Location: /admindashboard');
                    } else {
                        $this->view->render(NULL, NULL, 'adminlogin');
                    }
                } else {
                    $this->view->render(NULL, NULL, 'adminlogin');
                }

            } catch (CustomException $e){
                return $this->view->render(NULL, NULL, 'adminlogin', $e->setException());
            } catch(Throwable $t){
                log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            }
   }
}

?>