<?php

class admin_newpage_Controller extends Controller
{

    
   public function __construct()
   {
        parent::__construct();

            try {
               
                if(array_key_exists('submit', $_POST)) {

                    // uncomment it if u want to save only text
                   /* foreach($_POST as $key => $value){
                        Validate::cleanInput($value);
                    }*/

                    unset($_POST['submit']);

                    $this->admin_newpage = $this->model->setModel('admin_newpage', $_POST);

                   if($this->admin_newpage->result()) {
	                    $this->view->renderadmin(NULL, NULL, 'admin_newpage', (new CustomException(NULL, 0, NULL, 1026))->setException());
                   }
                    
                } else {
                    $this->view->renderadmin(NULL, NULL, 'admin_newpage');
                }

            } catch (CustomException $e){
                return $this->view->renderadmin(NULL, NULL, 'admin_newpage', $e->setException());
            } catch(Throwable $t){
                Log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            }
   }
}

?>