<?php
class addlink_Controller extends Controller
{
    
    public function __construct()
    {
        parent::__construct();
        //prepare categories from database for select element
        $this->categories = $this->model->getCategories('categories')['data'];
        foreach ($this->categories as $key => $value) {
            if($value['sub_cat']){
                $this->categories[$key]['subcategories'] = $this->model->getSubCategories($value['id'])['data'];
            }
        }

        try {
            if(array_key_exists('submit', $_POST)) {

                foreach($_POST as $key => $value){
                    Validate::cleanInput($value);
                }

                $this->usermsgs = array();
                if(Validate::isEmpty($_POST['link_title'])) {
                    $this->usermsgs[] = 1016;
                }
                if(Validate::isEmpty($_POST['link_url'])) {
                    $this->usermsgs[] = 1017;
                } 
                if(Validate::isEmpty($_POST['link_desc'])) {
                    $this->usermsgs[] = 1018;
                }
                if(Validate::isEmpty($_POST['link_keywords'])){
                    $this->usermsgs[] = 1019;
                }
                if(!Validate::isUrl($_POST['link_url'])){
                    $this->usermsgs[] = 1020;
                }
                if($_POST['category'] == ''){
                    $this->usermsgs[] = 1023;
                }
                if(!Validate::checkRecaptcha($_POST['g-recaptcha-response'])){
                    $this->usermsgs[] = 1029;
                }

                $count = count($this->usermsgs);

                if($count > 0){
                    throw new CustomException(NULL, 0, NULL, $this->usermsgs);
                }
                $catid = explode('-', $_POST['category']);
                //loop through the categories for slugs to the path
                $part = "";
                foreach ($this->categories as $key => $value) {
                    if($value['id'] == $catid[0]){
                        $part .= $value['slug'] . '/';
                    }
                    if($catid[1] != 0 && isset($value['subcategories'])){
                        foreach ($value['subcategories'] as $k => $v) {
                            if($v['id'] == $catid[1]){
                                $part .= $v['slug'] . '/';
                            }
                        }
                    }
                }
                $path = 'img/public/' . $part; 
                $servpath = PATH . $path;

                if(!is_dir($servpath)){
                    mkdir($servpath, 0777, TRUE);
                }
                $file = URL::url_slug($_POST['link_title']) . '.jpg';

                $fullpath = $servpath . $file;
                $imgsrc = Scrshot::getWss($_POST['link_url']);

                if(!fopen($fullpath, 'w')){
                    throw new Exception('Unable to save link image data: ' . $imgsrc);
                } else {
                    file_put_contents($fullpath, base64_decode($imgsrc)); 
                }

                $this->data = array('catid' => $catid[0],
                                    'subcatid' => $catid[1],
                                    'link_title' => $_POST['link_title'],
                                    'link_url' => $_POST['link_url'],
                                    'link_desc' => $_POST['link_desc'],
                                    'link_keywords' => $_POST['link_keywords'],
                                    'link_scrsht' => WEBSITE_URL . '/' . $path . $file,
                                    'username' => array_key_exists('username', $_SESSION) ? ($_SESSION['username']) : NULL,
                                    'link_date' => date("Y-m-d H:i:s"));
                                    
               $this->signup = $this->model->setModel('addlink', $this->data);

               if($this->signup->result()) {
                    $this->view->render(NULL, $this->categories, 'addlink', (new CustomException(NULL, 0, NULL, 1022))->setException()); //some normal landing page after login
               }
                
            } else {
                $this->view->render(NULL, $this->categories, 'addlink');
            }

        } catch (CustomException $e){
            return $this->view->render(NULL, $this->categories, 'addlink', $e->setException());
        } catch(Throwable $t){
            Log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
        }
}
}

?>