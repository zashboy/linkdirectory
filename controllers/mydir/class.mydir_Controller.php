<?php

class mydir_Controller extends Controller
{
    
    public function __construct()
    {
        parent::__construct();
        Authentication::handleLogin();

        $this->pageData = $this->model->getPage('mydir');

        $this->mydir = $this->model->setModel('mydir');

        if(isset($this->mydir)) {
            $this->preview($this->pageData, $this->mydir, 'mydir');
        } else {
            $this->view->render($this->pageData, $this->mydir, 'mydir');
        }
    }
}


?>