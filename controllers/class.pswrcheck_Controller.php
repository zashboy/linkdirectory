<?php
/**
* class.pswrcheck_Controller.php
*
* Handles the token from the password reset email
* and handles the new password creation
*
* @category   password reset check
* @package    Controller
* @author     zashboy
* @copyright  2018 zashboy
* @license    https://www.gnu.org/licenses/gpl-3.0.en.html
* @version    1.0
* @link       https://www.weblink.directory
* @see        class.passwordreset_Controller, class.passwordreset_Model, class.pswrcheck_Model
* @since      File available since Release 1.0
*/
class pswrcheck_Controller extends Controller
{

/**
 * 1.gets the token and pass it to the model after validation
 * @param string $_GET['token']
 * @return if validation ok render new password page
 * 2. validates the posted passwords and pass it to the model to save
 * @param string $_POST['password']
 * @return throw message on fail or success
 * 3. it throws fail message if there is no get or post data
 */
   public function __construct()
   {
        parent::__construct();

            try {

                if(array_key_exists('token', $_GET)) {

                    foreach($_GET as $key => $value){
                        Validate::cleanInput($value);
                    }
    
                    $this->data = array('token' => $_GET['token']);
                    $this->pswrcheck = $this->model->setModel('pswrcheck');

                    if($this->pswrcheck->checktoken($this->data)){
                        $this->view->render(NULL, NULL, 'newpsw');
                    }

                } elseif(array_key_exists('submit', $_POST)) {

                    $this->usermsgs = [];

                    if(Validate::isEmpty($_POST['psw'])) {
                        $this->usermsgs[] = 1002;
                    } 
                    if(Validate::isEmpty($_POST['repsw'])) {
                        $this->usermsgs[] = 1003;
                    }
                    if(!Validate::checkPsw($_POST['psw'])){
                        $this->usermsgs[] = 1006;
                    }
                    if(!Validate::checkPswsMatch($_POST['psw'], $_POST['repsw'])){
                        $this->usermsgs[] = 1007;
                    }

                    $count = count($this->usermsgs);

                    if($count > 0){
                        throw new CustomException(NULL, 0, NULL, $this->usermsgs);
                    }
                    $this->data2 = array('psw' => $_POST['psw']); 
                                        
                    $this->pswsave = $this->model->setModel('pswrcheck');

                    if($this->pswsave->savepsw($this->data2)) {
                            $this->view->render(NULL, NULL, 'pswrcheck', (new CustomException(NULL, 0, NULL, 1035))->setException());
                    }
                    
                } else {
                    $this->view->render(NULL, NULL, 'pswrcheck', (new CustomException(NULL, 0, NULL, 1036))->setException()); 
                }

            } catch (CustomException $e){
                return $this->view->render(NULL, NULL, 'pswrcheck', $e->setException());
            } catch(Throwable $t){
                log::user($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
            }
    }
}

?>