<?php
  $name = isset($_POST['name']) ? $_POST['name'] : NULL;
  $title = isset($_POST['title']) ? $_POST['title'] : NULL;
  $metadesc = isset($_POST['metadesc']) ? $_POST['metadesc'] : NULL;
  $metakey = isset($_POST['metakey']) ? $_POST['metakey'] : NULL;
  $content = isset($_POST['content']) ? $_POST['content'] : NULL;
  $sub_table = isset($_POST['sub_table']) ? $_POST['sub_table'] : NULL;
  
?>

    <section id="page-wrapper" class="">
        <div class="container">
            <div class="col-lg-8">
                <?= $alertmsg ?>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <h2 class="huge">New Page</h2>
                </div>
            </div>
            <div class="registration-form col-lg-10" id="newPage">
                <form class="form-horizontal" action="admin_newpage" method="post">
                    <div class="form-group">
                        <label class="control-label" for="name">Page uri:</label>
                        <div class="input-group">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Page name" value="<?= $name ?>" required>
                            <span class="input-group-addon" data-toggle="popover" title="Pagename requirements" data-content="This name should match with the uri"
                                data-placement="auto bottom" data-trigger="hover">
                                <i class="glyphicon glyphicon-info-sign"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="title">Page title:</label>
                        <div class="input-group">
                            <input type="text" name="title" class="form-control" id="title" placeholder="Page title" value="<?= $title ?>" required>
                            <span class="input-group-addon" data-toggle="popover" title="Page title requirements" data-content="It has to be a valid e-mail address because we send you an activation link to this address and you need to click on it before your first login"
                                data-placement="auto bottom" data-trigger="hover">
                                <i class="glyphicon glyphicon-info-sign"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="metadesc">Meta description:</label>
                        <div class="input-group">
                            <input type="text" name="metadesc" class="form-control" id="metadesc" placeholder="Meta description of the page" value="<?= $metadesc ?>">
                            <span class="input-group-addon" data-toggle="popover" title="Meta description requirements" data-content="Password must be at least 8 characters and must contain at least one lower case letter, one upper case letter and one digit"
                                data-placement="auto bottom" data-trigger="hover">
                                <i class="glyphicon glyphicon-info-sign"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="metakey">Meta keywords:</label>
                        <div class="input-group">
                            <input type="text" name="metakey" class="form-control" id="metakey" placeholder="Meta keywords of the page" value="<?= $metakey ?>">
                            <span class="input-group-addon" data-toggle="popover" title="Meta keywords" data-content="You need to re-type your password"
                                data-placement="auto bottom" data-trigger="hover">
                                <i class="glyphicon glyphicon-info-sign"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="content">Page content:</label>
                        <div class="input-group">
                            <textarea type="text" name="content" class="form-control" id="content" placeholder="Page content"><?= $content ?></textarea>
                            <span class="input-group-addon" data-toggle="popover" title="Page content requirements" data-content="You have to be 18 or older to use this website"
                                data-placement="auto bottom" data-trigger="hover">
                                <i class="glyphicon glyphicon-info-sign"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="sub_table">Sub table name:</label>
                        <div class="col-sm-4 input-group">
                            <input type="text" name="sub_table" class="form-control" id="sub_table" placeholder="The name of the sub table" value="<?= $sub_table ?>">
                            <span class="input-group-addon" data-toggle="popover" title="Sub table" data-content="If the page uses a table's content then the name of the table"
                                data-placement="auto bottom" data-trigger="hover">
                                <i class="glyphicon glyphicon-info-sign"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="submit" name="submit" class="btn btn-default">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
    </section>