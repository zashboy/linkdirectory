   <?php
   $extrajs = isset($js)?$js:NULL;
   ?>
   <!-- jQuery -->
    <script src="/vendor/jquery-3-3-1/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/vendor/bootstrap-3-3-7-admin/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="/vendor/raphael/raphael.min.js"></script>
    <script src="/vendor/morrisjs/morris.min.js"></script>
    <script src="/tmp/admin/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="tmp/admin/js/sb-admin-2.js"></script>
    
    <?= $extrajs ?>

    <script>
            $(document).ready(function(){
          $('.dropdown a.subDropdown').on("click", function(e){
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
          });
          $('[data-toggle="tooltip"]').tooltip();
          $('[data-toggle="popover"]').popover();  
        });

    </script>

</body>

</html>
