<?php if(is_array($exc)): ?>
  <?php foreach($exc as $value): ?>
<div class="alert <?= $value['style'] ?> alert-dismissible">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
    <p><b><?= $value['msg'] ?></b></p>
    <p><?= $value['details'] ?></p>
    
</div>
<?php endforeach; ?>
<?php else: ?>
<div class="alert <?= $exc['style'] ?> alert-dismissible">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
    <p><b><?= $exc['msg'] ?></b></p>
    <p><?= $exc['details'] ?></p>
</div>
<?php endif; ?>