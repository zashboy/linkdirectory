<div class="registration-form" id="signUp">
    <form class="form-horizontal" action="/signup" method="post">
        <h2>Registration</h2>
            <div class="form-group">
                <label class="control-label col-sm-2" for="usr">Username:</label>
                <div class="col-sm-8 input-group">
                    <input type="text" name="name" class="form-control" id="usr" placeholder="Enter your name" value="<?= $_POST['name'] ?>" required>
                    <div class="input-group-append">
                    <span data-toggle="popover" title="Username requirements" data-content="The username can contain letters, capital letters and numbers only" data-trigger="hover">
                        <i class="input-group-text">i</i>
                    </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="email">Email:</label>
                <div class="col-sm-8 input-group">
                    <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="<?= $_POST['email'] ?>" required>
                    <div class="input-group-append">
                    <span data-toggle="popover" title="E-mail requirements" data-content="It has to be a valid e-mail address because we send you an activation link to this address and you need to click on it before your first login" data-trigger="hover">
                        <i class="input-group-text">i</i>
                    </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="psw">Password:</label>
                <div class="col-sm-4 input-group">
                    <input type="password" name="psw" class="form-control" id="psw" placeholder="Enter your password" required>
                    <div class="input-group-append">
                    <span data-toggle="popover" title="Password requirements" data-content="Password must be at least 8 characters and must contain at least one lower case letter, one upper case letter and one digit" data-trigger="hover">
                        <i class="input-group-text">i</i>
                    </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="repsw">Confirm password:</label>
                <div class="col-sm-4 input-group">
                    <input type="password" name="repsw" class="form-control" id="repsw" placeholder="Re-enter your password" required>
                    <div class="input-group-append">
                    <span data-toggle="popover" title="Re-type password" data-content="You need to re-type your password" data-trigger="hover">
                        <i class="input-group-text">i</i>
                    </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="bday">Date of birth:</label>
                <div class="col-sm-3 input-group">
                    <input type="date" name="bday" class="form-control" id="bday" placeholder="dd-mm-yyyy" value="<?= $_POST['bday'] ?>" required>
                    <div class="input-group-append">
                    <span data-toggle="popover" title="Date of birth requirements" data-content="You have to be 18 or older to use this website" data-trigger="hover">
                        <i class="input-group-text">i</i>
                    </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="captcha">Captcha:</label>
                <div class="col-sm-8 input-group">
                    <div class="g-recaptcha" data-sitekey="6Lf0zDoUAAAAAA4BIbaV9G18J1UMs7G6aepGania"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <button type="submit" name="submit" class="btn btn-default">Submit</button>
                </div>
            </div>
    </form>
</div>