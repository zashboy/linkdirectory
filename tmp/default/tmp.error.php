<div class="jumbotron jumbotron-fluid">
  <div class="container <?= $metadata[0]['bg'] ?>">
    <h1><?= $msg ?></h1>
    <p><?= $metadata[0]['details'] ?></p>
  </div>
</div>