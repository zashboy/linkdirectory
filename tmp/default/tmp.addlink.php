<div class="registration-form" id="addLink">
    <form class="form-horizontal" action="/addlink" method="post">
        <h2>Add Link</h2>
        <div class="form-group">
            <label class="control-label col-sm-2" for="category">Category:</label>
            <div class="col-sm-8 input-group">
                <?php $val = isset($_POST['category'])?$_POST['category']:'none'; ?>
                <?php $pid = explode('-', $_POST['category']); ?>
                <?php $cat_title = isset($_POST['category'])?(($pid[1]==0)?$data[$pid[0]-1]['title']:$data[$pid[0]]['subcategories'][$pid[1]-1]['title']):'Choose one category'; ?>
                <select name="category">
                <option value="<?= $val ?>" selected><?= $cat_title ?></option>
                <?php foreach($data as $arr): ?>
                    <option value="<?= $arr['id']. '-' .'0' ?>"><?= $arr['title'] ?></option>
                    <?php if(array_key_exists('subcategories', $arr)): ?>
                    <?php foreach($arr['subcategories'] as $_arr): ?>
                        <option value="<?= $arr['id']. '-' .$_arr['id'] ?>">--<?= $_arr['title'] ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                </select>
                <div class="input-group-append">
                    <span data-toggle="popover" title="Category requirements" data-content="Choose one category"
                        data-trigger="hover">
                        <i class="input-group-text">i</i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="link_title">Link title:</label>
            <div class="col-sm-8 input-group">
                <input type="text" name="link_title" class="form-control" id="link_title" placeholder="Link title" value="<?= $_POST['link_title'] ?>"
                    required>
                <div class="input-group-append">
                    <span data-toggle="popover" title="Link title requirements" data-content="It can contain only letters, numbers and whitespaces"
                        data-trigger="hover">
                        <i class="input-group-text">i</i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="link_url">Link URL</label>
            <div class="col-sm-4 input-group">
                <input type="url" name="link_url" class="form-control" id="link_url" placeholder="Link URL" value="<?= $_POST['link_url'] ?>" required>
                <div class="input-group-append">
                    <span data-toggle="popover" title="Link URL requirements" data-content="It has to match to the url format eg: http://www.weblink.directory"
                        data-trigger="hover">
                        <i class="input-group-text">i</i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="link_desc">Link description:</label>
            <div class="col-sm-10 input-group">
                <textarea name="link_desc" class="form-control" id="link_desc" required><?= $_POST['link_desc'] ?></textarea>
                <div class="input-group-append">
                    <span data-toggle="popover" title="Link description" data-content="Write a description to the link what you add" data-trigger="hover">
                        <i class="input-group-text">i</i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="link_keywords">Keywords:</label>
            <div class="col-sm-3 input-group">
                <input type="text" name="link_keywords" class="form-control" id="link_keywords" placeholder="Keywords" value="<?= $_POST['link_keywords'] ?>" required>
                <div class="input-group-append">
                    <span data-toggle="popover" title="Keywords requirements" data-content="Type a couple of keywords separate them by commas"
                        data-trigger="hover">
                        <i class="input-group-text">i</i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="captcha">Captcha:</label>
            <div class="col-sm-8 input-group">
                <div class="g-recaptcha" data-sitekey="6Lf0zDoUAAAAAA4BIbaV9G18J1UMs7G6aepGania"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <button type="submit" name="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
    </form>
</div>