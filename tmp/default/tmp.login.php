<div style="margin: auto" class="center-block login-form" id="login">
<form class="form-horizontal text-center" action="login" method="post">
<h2>Login</h2>
  <div class="form-group">
  <label class="control-label col-sm-2" for="usr">Username:</label>
  <div class="col-sm-10">
    <input type="text" name="name" class="form-control" id="name" placeholder="Enter your name" value="" required>
  </div>
  </div>
  <div class="form-group">
  <label class="control-label col-sm-2" for="psw">Password:</label>
    <div class="col-sm-10">
      <input type="password" name="psw" class="form-control" id="psw" placeholder="Enter your password" value="" required>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-1 col-sm-10">
      <button type="submit" name="submit" class="btn btn-default">Login</button>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-1 col-sm-10">
    <button type="button" class="btn btn-link" onclick="window.location='registration'">Sign Up</button>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-1 col-sm-10">
    <button type="button" class="btn btn-link" onclick="window.location='password-reset'">I forgot my password</button>
    </div>
  </div>
</form>
</div>