<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?=$metadata[0]['metadesc']?>">
  <meta name="keywords" content="<?=$metadata[0]['metaKey']?>">
  <title>
    <?=ucfirst($metadata[0]['title'])?>
  </title>
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <!-- My style -->
  <link rel="stylesheet" href="/tmp/default/css/style.css?v=1.5">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
</head>

<body>
  <div class="container">
    <noscript>
      <p class="alert alert-danger" style="margin-top:5%;">You don't have javascript enabled! You need to enable it to use this website!</p>
    </noscript>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon">
            </span>
          </button>
          <a class="navbar-brand" href="/">Weblink Directory</a>
        </div>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="/">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/categories">Categories</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/about">About</a>
            </li>
          <li class="nav-item">
          <form id="search" class="form-inline" action="/action_page.php">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search">
              <div class="input-group-append">
                <span class="input-group-text">GO</span>
              </div>
            </div>
          </form>
        </li>
        </ul>
        </div>
        <?php $this->loggedinMenu(); ?> 
      </div>
    </nav>
    <div class="container">