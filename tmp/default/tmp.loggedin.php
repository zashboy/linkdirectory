<ul class="nav navbar-nav navbar-right">
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="/signup" id="navbardrop" data-toggle="dropdown">
            Menu</a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="/addlink">Add link</a>
            <a class="dropdown-item" href="#">Link 2</a>
            <a class="dropdown-item" href="#">Link 3</a>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/logout">
            Logout</a>
    </li>
</ul>