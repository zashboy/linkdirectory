    <!-- About -->
    <section id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">About</h2>
            <h3 class="section-subheading text-muted">Our story.</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <ul class="timeline">
              <li>
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="/tmp/agency/img/about/1.jpg" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4>2009-2012</h4>
                    <h4 class="subheading">Our Humble Beginnings</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted">In 2009 a hungarian domain (linkeld.hu) has been registered by me with similar goals. It worked mostly as a link exchange system for a couple of years but when the circumstances have been changed I closed its doors in 2012.</p>
                  </div>
                </div>
              </li>
              <li>
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="/tmp/agency/img/about/3.jpg" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4>2012-2017</h4>
                    <h4 class="subheading">Other Projects Written by Life</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted">Nearly five years have gone after the parent site closed and in these five years my days were too busy to build up a new life there was no time to code and maintain any websites</p>
                  </div>
                </div>
              </li>
              <li class="timeline-inverted">
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="/tmp/agency/img/about/4.jpg" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4>October 2017</h4>
                    <h4 class="subheading">New start</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted">I just couldn't forget my love with coding I had to do something again, get back to my hobby and the first idea was to build a linkd irectory. Yes, link directories are not very popular nowadays so probably it's late a bit but maybe we can do something to get them back to the stage... so this month I've registered the <a href="/">Weblink.directory</a> domain.</p>
                  </div>
                </div>
              </li>
              <li class="timeline-inverted">
                <div class="timeline-image">
                  <h4>Be Part
                    <br>Of Our
                    <br>Story!</h4>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
