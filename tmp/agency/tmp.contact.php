<?php
  $name = isset($_POST['name']) ? $_POST['name'] : NULL;
  $email = isset($_POST['email']) ? $_POST['email'] : NULL;
  $message = isset($_POST['message']) ? $_POST['message'] : NULL;
?>

  <!-- Contact -->
  <section id="contact">
    <div class="container">
      <div class="col-lg-12">
        <?= $alertmsg ?>
      </div>
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Contact Us</h2>
          <h3 class="section-subheading text-muted">The easiest way to send us a message to fill out the form below</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <form id="contactForm" name="sentMessage" action="/contact" novalidate="novalidate" method="post">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input class="form-control" name="name" id="name" type="text" placeholder="Your Name *" required="required" data-validation-required-message="Please enter your name."
                    value="<?= $name ?>">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" name="email" id="email" type="email" placeholder="Your Email *" required="required" data-validation-required-message="Please enter your email address."
                    value="<?= $email ?>">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <div class="col-sm-8 input-group">
                  <div class="g-recaptcha" data-sitekey="<?= RECAPTCHA_SITE_KEY ?>"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <textarea class="form-control" name="message" id="message" placeholder="Your Message *" required="required" data-validation-required-message="Please enter a message."><?= $message ?></textarea>
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-lg-12 text-center">
                <button class="btn btn-primary btn-xl text-uppercase" name="submit" type="submit">Send Message</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>