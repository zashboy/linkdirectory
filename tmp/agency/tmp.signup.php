<?php
  $name = isset($_POST['name']) ? $_POST['name'] : NULL;
  $email = isset($_POST['email']) ? $_POST['email'] : NULL;
  $bday = isset($_POST['bday']) ? $_POST['bday'] : NULL;
?>

    <section id="contact">
        <div class="container">
            <div class="col-lg-12">
                <?= $alertmsg ?>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">Sign up</h2>
                    <h3 class="section-subheading text-muted">Make an account to use all the functions of this website</h3>
                </div>
            </div>
            <div class="row d-flex justify-content-center ">
                <div class="col-lg-12">
                    <form id="signupForm" class="form-horizontal" action="/signup" method="post" novalidate="novalidate">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" name="name" class="form-control" id="usr" placeholder="Enter your name" data-validation-required-message="Please enter your name."
                                    value="<?= $name ?>" required>
                                <div class="input-group-append">
                                    <span data-toggle="popover" title="Username requirements" data-content="The username can contain letters, capital letters and numbers only"
                                        data-trigger="hover">
                                        <i class="input-group-text">i</i>
                                    </span>
                                </div>
                            </div>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" data-validation-required-message="Please enter your email address."
                                    value="<?= $email ?>" required>
                                <div class="input-group-append">
                                    <span data-toggle="popover" title="E-mail requirements" data-content="It has to be a valid e-mail address because we send you an activation link to this address and you need to click on it before your first login"
                                        data-trigger="hover">
                                        <i class="input-group-text">i</i>
                                    </span>
                                </div>
                            </div>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="password" name="psw" class="form-control" id="psw" placeholder="Enter your password" data-validation-required-message="Please enter your password."
                                    required>
                                <div class="input-group-append">
                                    <span data-toggle="popover" title="Password requirements" data-content="Password must be at least 8 characters and must contain at least one lower case letter, one upper case letter and one digit"
                                        data-trigger="hover">
                                        <i class="input-group-text">i</i>
                                    </span>
                                </div>
                            </div>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="password" name="repsw" class="form-control" id="repsw" placeholder="Re-enter your password" data-validation-match-match="psw"
                                    data-validation-required-message="Please retype your password" required>
                                <div class="input-group-append">
                                    <span data-toggle="popover" title="Re-type password" data-content="You need to re-type your password" data-trigger="hover">
                                        <i class="input-group-text">i</i>
                                    </span>
                                </div>
                            </div>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="date" name="bday" class="form-control" id="bday" placeholder="dd-mm-yyyy" value="<?= $bday ?>">
                                <div class="input-group-append">
                                    <span data-toggle="popover" title="Date of birth requirements" data-content="If you are older than 18 you can see the adult content"
                                        data-trigger="hover">
                                        <i class="input-group-text">i</i>
                                    </span>
                                </div>
                            </div>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 input-group">
                                <div class="g-recaptcha" data-sitekey="<?= RECAPTCHA_SITE_KEY ?>"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div>
                                <button type="submit" name="submit" class="btn btn-primary btn-xl text-uppercase">Sign-up</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>