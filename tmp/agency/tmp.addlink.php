<?php
  $val = $pid = $cat_title = "";
  $val = isset($_POST['category'])?$_POST['category']:NULL;
  $pid = explode('-', $val);
  //subcat doesnt work..
  $cat_title = isset($_POST['category'])?(($pid[1]==0)?$data[$pid[0]-1]['title']:$data[$pid[0]]['subcategories'][$pid[1]-1]['title']):'Choose one category';

  $link_title = isset($_POST['link_title']) ? $_POST['link_title'] : NULL;
  $link_url = isset($_POST['link_url']) ? $_POST['link_url'] : NULL;
  $link_keywords = isset($_POST['link_keywords']) ? $_POST['link_keywords'] : NULL;
  $link_desc = isset($_POST['link_desc']) ? $_POST['link_desc'] : NULL;
?>

    <section id="contact">
        <div class="container">
            <div class="col-lg-12">
                <?= $alertmsg ?>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">Add link</h2>
                    <h3 class="section-subheading text-muted">Add a link to the directory</h3>
                </div>
            </div>
            <div class="row d-flex justify-content-center ">
                <div class="col-lg-12">
                    <form id="addlinkForm" class="form-horizontal" action="/addlink" method="post" novalidate="novalidate">
                        <div class="form-group">
                            <div class="input-group">
                                <select name="category" class="form-control" data-validation-required-message="Please choose one category." required="required">
                                    <option value="<?= $val ?>" selected>
                                        <?= $cat_title ?>
                                    </option>
                                    <?php foreach($data as $arr): ?>
                                    <option value="<?= $arr['id']. '-' .'0' ?>">
                                        <?= $arr['title'] ?>
                                    </option>
                                    <?php if(array_key_exists('subcategories', $arr)): ?>
                                    <?php foreach($arr['subcategories'] as $_arr): ?>
                                    <option value="<?= $arr['id']. '-' .$_arr['id'] ?>">--
                                        <?= $_arr['title'] ?>
                                    </option>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                                <div class="input-group-append">
                                    <span data-toggle="popover" title="Category requirements" data-content="Choose one category" data-trigger="hover">
                                        <i class="input-group-text">i</i>
                                    </span>
                                </div>
                            </div>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" name="link_title" class="form-control" id="link_title" placeholder="Link title" data-validation-required-message="Please enter your link's title."
                                    value="<?= $link_title ?>" required>
                                <div class="input-group-append">
                                    <span data-toggle="popover" title="Link title requirements" data-content="It can contain only letters, numbers and whitespaces"
                                        data-trigger="hover">
                                        <i class="input-group-text">i</i>
                                    </span>
                                </div>
                            </div>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="url" name="link_url" class="form-control" id="link_url" placeholder="Link URL eg.: http://www.weblink.directory"
                                    data-validation-required-message="Please enter your link's URL." value="<?= $link_url ?>"
                                    required>
                                <div class="input-group-append">
                                    <span data-toggle="popover" title="Link URL requirements" data-content="It has to match to the url format eg: http://www.weblink.directory"
                                        data-trigger="hover">
                                        <i class="input-group-text">i</i>
                                    </span>
                                </div>
                            </div>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" name="link_keywords" class="form-control" id="link_keywords" placeholder="Keywords" data-validation-required-message="Please enter some keywords."
                                    value="<?= $link_keywords ?>" required>
                                <div class="input-group-append">
                                    <span data-toggle="popover" title="Keywords requirements" data-content="Type a couple of keywords separate them by commas"
                                        data-trigger="hover">
                                        <i class="input-group-text">i</i>
                                    </span>
                                </div>
                            </div>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <textarea name="link_desc" class="form-control" id="link_desc" placeholder="Link description" data-validation-required-message="Please enter your link's description." required><?= $link_desc ?></textarea>
                                <div class="input-group-append">
                                    <span data-toggle="popover" title="Link description" data-content="Write a description to the link what you add" data-trigger="hover">
                                        <i class="input-group-text" id="textarea">i</i>
                                    </span>
                                </div>
                            </div>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 input-group">
                                <div class="g-recaptcha" data-sitekey="<?= RECAPTCHA_SITE_KEY ?>"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-8">
                                <button type="submit" name="submit" class="btn btn-primary btn-xl text-uppercase">Add link</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>