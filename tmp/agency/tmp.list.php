<?php
$p = isset($metadata[0]['slug']) ? $metadata[0]['slug'].'/' : NULL;
$catdesc = isset($metadata[0]['desc']) ? $metadata[0]['desc'] : NULL;
$title = isset($metadata[0]['title']) ? $metadata[0]['title'] : NULL;
?>
<section id="portfolio">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase"><?= $title ?></h2>
            <h3 class="section-subheading text-muted"><?= $catdesc ?></h3>
          </div>
        </div>
        <div class="row">
        <?php foreach($data as $arr) : ?>
        <div class="col-md-4 col-sm-6 portfolio-item">
        <a class="portfolio-link" href="/categories/<?= $p ?><?= $arr['slug'] ?>">
        <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
        <img class="img-fluid" src="/tmp/agency/img/categories/<?= $p ?><?= $arr['slug'] ?>.jpg" alt=""></a>
        <div class="portfolio-caption">
            <a href="/categories/<?= $p ?><?= $arr['slug'] ?>">
            <h4><?= $arr['title'] ?></h4>
            </a>
            <?php $desc = isset($arr['desc']) ? $arr['desc'] : NULL; ?>
            <p class="text-muted"><?= $desc ?></p>
            </div>
          </div>
        <?php endforeach; ?>
        </div>
      </div>
    </section>