<?php
  $desc = $metakey = $title = "";
  $desc = isset($metadata[0]['metadesc']) ? $metadata[0]['metadesc'] : 'Weblink directory';
  $metakey = isset($metadata[0]['metakey']) ? $metadata[0]['metakey'] : 'Weblink, directory';
  $title = isset($metadata[0]['title']) ? $metadata[0]['title'] : 'Weblink directory';
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?= $desc ?>">
  <meta name="keywords" content="<?= $metakey ?>">
  <title>
    <?=ucfirst($title)?>
  </title>
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
  <!-- Bootstrap core CSS -->
  <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#ffc40d">
  <meta name="theme-color" content="#ffffff">

  <!-- Custom fonts for this template -->
  <link href="/vendor/font-awesome-4-7-0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="/tmp/agency/css/agency.min.css" rel="stylesheet">
  <?= $css ?> 

  <!-- Google Recaptcha -->
  <script src='https://www.google.com/recaptcha/api.js'></script>

  </head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="/home">Weblink Directory</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive"
        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fa fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ml-auto">
        <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="/home">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="/categories">Categories</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="/addlink">Add link</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="/about">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="/contact">Contact</a>
          </li>
        </ul>
        <?php $this->loggedinMenu(); ?> 
      </div>
    </div>
  </nav>