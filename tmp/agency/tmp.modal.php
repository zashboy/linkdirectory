<?php $favicon = @fopen($linkdata['link_url'] . '/favicon.ico', "r") ? $linkdata['link_url'] . '/favicon.ico' : NULL; ?>
<!-- Modal <?= $linkdata['id'] ?> -->
<div class="portfolio-modal modal fade" id="linkmodal<?= $linkdata['id'] ?>" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="close-modal" data-dismiss="modal">
        <div class="lr">
          <div class="rl"></div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <div class="modal-body">
              <!-- Project Details Go Here -->
              <h2 class="text-uppercase">
                <a href="<?= $linkdata['link_url'] ?>">
                  <?= $linkdata['link_title'] ?>
                </a>
              </h2>
              <p class="item-intro text-muted">
                <a href="<?= $linkdata['link_url'] ?>">
                <img class="img-fluid" style="min-height:30px;min-width:30px;" src="<?= $favicon ?>" alt="">
                  <?= $linkdata['link_url'] ?>
                </a>
              </p>
              <a href="<?= $linkdata['link_url'] ?>">
                <img class="img-fluid d-block mx-auto" src="<?= $linkdata['link_scrsht'] ?>" alt="<?= $linkdata['link_keywords'] ?>">
              </a>
              <p>
                <?= $linkdata['link_desc'] ?>
              </p>
              <ul class="list-inline">
                <li>Added:
                  <?= $linkdata['link_date'] ?>
                </li>
              </ul>
              <button class="btn btn-primary" data-dismiss="modal" type="button">
                <i class="fa fa-times"></i>
                Close linkpage</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>