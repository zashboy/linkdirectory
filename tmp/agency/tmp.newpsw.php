
<?php
  $name = isset($_POST['name']) ? $_POST['name'] : NULL;
  $email = isset($_POST['email']) ? $_POST['email'] : NULL;
  $bday = isset($_POST['bday']) ? $_POST['bday'] : NULL;
?>

    <section id="contact">
        <div class="container">
            <div class="col-lg-12">
                <?= $alertmsg ?>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">New Password</h2>
                    <h3 class="section-subheading text-muted">Please type your new password below</h3>
                </div>
            </div>
            <div class="row d-flex justify-content-center ">
                <div class="col-lg-12">
                    <form id="signupForm" class="form-horizontal" action="/pswrcheck" method="post" novalidate="novalidate">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="password" name="psw" class="form-control" id="psw" placeholder="Enter your password" data-validation-required-message="Please enter your new password."
                                    required>
                                <div class="input-group-append">
                                    <span data-toggle="popover" title="Password requirements" data-content="Password must be at least 8 characters and must contain at least one lower case letter, one upper case letter and one digit"
                                        data-trigger="hover">
                                        <i class="input-group-text">i</i>
                                    </span>
                                </div>
                            </div>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="password" name="repsw" class="form-control" id="repsw" placeholder="Re-enter your new password" data-validation-match-match="psw"
                                    data-validation-required-message="Please retype your new password" required>
                                <div class="input-group-append">
                                    <span data-toggle="popover" title="Re-type new password" data-content="You need to re-type your new password" data-trigger="hover">
                                        <i class="input-group-text">i</i>
                                    </span>
                                </div>
                            </div>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <div>
                                <button type="submit" name="submit" class="btn btn-primary btn-xl text-uppercase">Save password</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>