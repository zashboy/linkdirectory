<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title><?= WEBSITE_NAME ?>, You changed your password successfully</title>
</head>
<body>
<h3>Dear <?= $to['name'] ?>,</h3>
</br>
<p>You successfully changed your password on <a href="<?= WEBSITE_URL ?>"><?= WEBSITE_NAME ?></a></p>
<p></p>
</br>
<p>If it wasn't you, please contact us as soon as possible.</p>
<a href="<?= WEBSITE_URL ?>/contact">Click on this link to contact us.</a>
</br>
<p>Or write an email to the address below.</p>
<blockquote>
   <pre>
       <code>
       <?= WEBSITE_REPLYTO_EMAIL ?>
       </code>
    </pre>
</blockquote>
</br>
<p>Thank You.</p> 
<p>Keep enjoying the best content with us.</p>
</br>
</br>
<p>Regards,</p>
<p><a href="<?= WEBSITE_URL ?>"><?= WEBSITE_NAME ?></a></p>
</body>
</html>