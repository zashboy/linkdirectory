<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title><?= WEBSITE_NAME ?>, Account activation</title>
</head>
<body>
<h3>Dear <?= $to['name'] ?>,</h3>
</br>
<p>Someone, hopefully You, tried to registrate an account on <a href="<?= WEBSITE_URL ?>"><?= WEBSITE_NAME ?></a></p>
<p>You have to verify Your e-mail address, before you login.</p>
</br>
<p>To finish the registration process you need to click on the activation link below.</p>
<a href="<?= WEBSITE_URL ?>/activation/?token=<?= $to['token'] ?>">Click on this link to activate your account.</a>
</br>
<p>Or copy this link to your browsers addressbar.</p>
<blockquote>
   <pre>
       <code>
       <?= WEBSITE_URL ?>/activation/?token=<?= $to['token'] ?>
       </code>
    </pre>
</blockquote>
</br>
<p>Thank You.</p> 
<p>Keep enjoying the best videos with us.</p>
</br>
<p>If You haven't tried to make an account on our site, please, ignore this message.</p>
</br>
<p>Regards,</p>
<p><a href="<?= WEBSITE_URL ?>"><?= WEBSITE_NAME ?></a></p>
</body>
</html>