<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title><?= WEBSITE_NAME ?>, Password recovery</title>
</head>
<body>
<h3>Dear <?= $to['name'] ?>,</h3>
</br>
<p>Someone, hopefully You, tried to recover your account on <a href="<?= WEBSITE_URL ?>"><?= WEBSITE_NAME ?></a></p>
<p>You can reset your password if you click on the link below or copy it to the address bar into your browser.</p>
</br>
<p>Please click on the link below and follow the instructions on the webpage.</p>
<a href="<?= WEBSITE_URL ?>/pswrcheck/?token=<?= $to['token'] ?>">Click on this link to recover your account.</a>
</br>
<p>Or copy this link to your browser's address bar.</p>
<blockquote>
   <pre>
       <code>
       <?= WEBSITE_URL ?>/pswrcheck/?token=<?= $to['token'] ?>
       </code>
    </pre>
</blockquote>
</br>
<p>Thank You.</p> 
<p>Keep enjoying the best content with us.</p>
</br>
<p>If You haven't tried to reset your password on our site, please, ignore this message.</p>
</br>
<p>Regards,</p>
<p><a href="<?= WEBSITE_URL ?>"><?= WEBSITE_NAME ?></a></p>
</body>
</html>