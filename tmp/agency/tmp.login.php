<section id="contact">
  <div class="container">
  <div class="col-lg-12"><?= $alertmsg ?></div>
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading text-uppercase">Login</h2>
        <h3 class="section-subheading text-muted">Don't you have an accout? <a href="/signup">Make one right now.</a></h3>
      </div>
    </div>
    <div class="row d-flex justify-content-center">
      <div class="col-lg-6">
        <form class="form-horizontal text-center" action="/login" method="post">
          <div class="form-group">
              <input type="text" name="name" class="form-control" id="name" placeholder="Username" value="" required>
          </div>
          <div class="form-group">
              <input type="password" name="psw" class="form-control" id="psw" placeholder="Password" value="" required>
          <input type="hidden" name="ref" value="<?= $_GET['ref'] ?>" >
          </div>
          <div class="form-group">
              <button type="submit" name="submit" class="btn btn-primary btn-xl text-uppercase">Login</button>
          </div>
          <div class="form-group">
              <button type="button" class="btn btn-link" onclick="window.location='signup'">Sign Up</button>
          </div>
          <div class="form-group">
              <button type="button" class="btn btn-link" onclick="window.location='passwordreset'">I forgot my password</button>
          </div>
        </form>
        </div>
        </div>
      </div>
    </section>