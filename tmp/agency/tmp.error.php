<section id="portfolio">
<div class="container" style="margin-top:10%">
  <div class="row <?= $metadata[0]['bg'] ?>">
    <div class="col-lg-12 text-center">
      <h2 class="section-heading text-uppercase">
        <?= $data ?>
      </h2>
      <h3 class="section-subheading text-muted">
        <?= $metadata[0]['details'] ?>
      </h3>
    </div>
  </div>
</div>
</section> 