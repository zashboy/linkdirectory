(function() {
    //"use strict"; // Start of use strict
    function isset(data) {
        if (typeof data === 'undefined') {
            return false;
        } else {
            if(data){
                return true;
            } else {
                return false;
            }
        }
    }
    $("#addMyCategory input").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
        // additional error messages or events
        },
        submitSuccess: function($form, event) {
        event.preventDefault(); // prevent default submit behaviour
        // get values from FORM
        var title = $("input#title").val();
        var desc = $("input#desc").val();

        $this = $("#sendMessageButton");
        $this.prop("disabled", true); // Disable submit button until AJAX call is complete to prevent duplicate messages
        $.ajax({
            url: "/ajax_mydir.php",
            type: "POST",
            data: {
            title: title,
            description: desc
            },
            dataType: 'json',
            success: function(response) {
                console.log(response);
            
            // Success message
            $('#success').html("<div class='alert alert-success'>");
            $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                .append("</button>"); 
            $('#success > .alert-success')
                .append("<strong>Your message has been sent. </strong>");
            $('#success > .alert-success')
                .append('</div>');

            var cats = "<div>";
            $.each(response, function(key, value){
                if(isset(value.title)){
                    cats += "<p>" + value.title + "</p>";
                }
            });
            cats += "</div>";
            $('.categories').append(cats);
            
            //clear all fields
            $('#contactForm').trigger("reset");
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            // Fail message
            $('#success').html("<div class='alert alert-danger'>");
            $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                .append("</button>");
            $('#success > .alert-danger').append($("<strong>").text("Sorry Mate , it seems that my mail server is not responding. Please try again later!"));
            $('#success > .alert-danger').append('</div>');
            //clear all fields
            $('#contactForm').trigger("reset");
            },
            complete: function() {
            setTimeout(function() {
                $this.prop("disabled", false); // Re-enable submit button when AJAX call is complete
            }, 1000);
            }
        });
        },
        filter: function() {
        return $(this).is(":visible");
        },
    });
    
    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
    
    /*When clicking on Full hide fail/success boxes */
    $('#name').focus(function() {
    $('#success').html('');
    });

    /*
    $(document).on("click", "#favv", function(){
        var favv = $(this).attr("data-fav-vidid");
        $.post("/templates/data_handler.php",
         {"favvidA":[{"vid":favv, "type":"<?= $type ?>"}]},
         function(data){
          $('[data-fav-vidid='+data+'][data-toggle="tooltip"]').attr({"id":"favvsaved","data-original-title":"Saved to favourites, click again to delete it"});
        });
      });
      /* delete saved vid */
      /*
    $(document).on("click", "#favvsaved", function(){
        var favVDel = $(this).attr("data-fav-vidid");
        $.post("/templates/data_handler.php",{"favVDel":[{"vid":favVDel, "type":"<?= $type ?>"}]},function(data){
          $('[data-fav-vidid='+data+'][data-toggle="tooltip"]').attr({"id":"favv","data-original-title":"Click to save this video to your favourites list"});
          location.reload();
        });
      });*/

})(jQuery); // End of use strict
  