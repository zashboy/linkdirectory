    <!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in">Welcome To Our Linkdirectory!</div>
          <div class="intro-heading text-uppercase">Publish website or save private bookmarks.</div>
          <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">Tell Me More</a>
        </div>
      </div>
    </header>

    <!-- Services -->
    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Services</h2>
            <h3 class="section-subheading text-muted">Link directory where you can have your private and public link collection.</h3>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-bookmark fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Public Linkdirectory</h4>
            <p class="text-muted">There is no easier way to get a backlink to your website from a clean source.</p>
          </div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Private Bookmarks</h4>
            <p class="text-muted">You can save your bookmarks, favorite links to a safe place where only you can see them if you login to our site.</p>
          </div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Responsive Design</h4>
            <p class="text-muted">The website's design is optimalised to every screensize so you can use it conveniently on all of your devices.</p>
          </div>
        </div>
      </div>
    </section>
