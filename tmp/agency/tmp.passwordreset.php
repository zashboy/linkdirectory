<?php
  $email = isset($_POST['email']) ? $_POST['email'] : NULL;
?>

    <section id="contact">
        <div class="container">
            <div class="col-lg-12">
                <?= $alertmsg ?>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">Password recovery</h2>
                    <h3 class="section-subheading text-muted">If you forgot your password, please, give us your email address and we send you a message with the recovery
                        instructions.</h3>
                </div>
            </div>
            <div class="row d-flex justify-content-center ">
                <div class="col-lg-12">
                    <form id="signupForm" class="form-horizontal" action="/passwordreset" method="post" novalidate="novalidate">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" data-validation-required-message="Please enter your email address."
                                    value="<?= $email ?>" required>
                                <div class="input-group-append">
                                    <span data-toggle="popover" title="E-mail requirements" data-content="It has to be a valid e-mail address because we send you an email with the instructions to recover your password"
                                        data-trigger="hover">
                                        <i class="input-group-text">i</i>
                                    </span>
                                </div>
                            </div>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 input-group">
                                <div class="g-recaptcha" data-sitekey="<?= RECAPTCHA_SITE_KEY ?>"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div>
                                <button type="submit" name="submit" class="btn btn-primary btn-xl text-uppercase">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>