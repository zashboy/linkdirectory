<?php
?>
<section id="portfolio">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading text-uppercase">Links</h2>
        <h3 class="section-subheading text-muted">These are the links in this category</h3>
      </div>
    </div>
    <div class="row d-flex justify-content-center">
      <?php $p = isset($metadata[0]['slug']) ? $metadata[0]['slug'].'/' : NULL ?>
      <?php foreach($data as $arr) : ?>
      <div class="col-md-4 col-sm-6 portfolio-item">
        <a class="portfolio-link"  data-toggle="modal" href="#linkmodal<?= $arr['id'] ?>">
          <div class="portfolio-hover">
            <div class="portfolio-hover-content">
              <i class="fa fa-plus fa-3x"></i>
            </div>
          </div>
          <img class="img-fluid" src="<?= $arr['link_scrsht'] ?>" alt="">
        </a>
        <div class="portfolio-caption">
          <?php $favicon = @fopen($arr['link_url'] . '/favicon.ico', "r") ? $arr['link_url'] . '/favicon.ico' : NULL; ?>
          <a href="<?= $arr['link_url'] ?>">
            <h4>
              <img class="img-fluid" src="<?= $favicon ?>" alt="">
              <a  data-toggle="modal" href="#linkmodal<?= $arr['id'] ?>">
                <?= $arr['link_title'] ?>
              </a>
            </h4>
            <p class="text-muted"><?= $arr['link_keywords']?></p>
        </div>
      </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>