    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; <?= WEBSITE_NAME ?> 2018</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="https://twitter.com/WeblinkD">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="https://www.facebook.com/WeblinkDirectory">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="https://plus.google.com/b/101682641417298398647/">
                  <i class="fa fa-google-plus"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    <!-- Modals -->
    <?php if(isset($domodals)): ?>
    <?php foreach($domodals as $modals): ?>    
      <?= $modals ?>
    <?php endforeach; ?>
    <?php endif; ?>    

    <!-- Bootstrap core JavaScript -->
    <script src="/vendor/jquery-3-3-1/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="/vendor/jquery-easing-1-4-1/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="/tmp/agency/js/jqBootstrapValidation.js"></script>

    <!-- Custom scripts for this template -->
    <script src="/tmp/agency/js/agency.min.js"></script>

    <?= $js ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109582050-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-109582050-2');
</script>

  </body>

</html>