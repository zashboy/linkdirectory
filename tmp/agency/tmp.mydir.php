<?php
$mytitle = NULL;
$mydesc = NULL;

?>
<section id="portfolio">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading text-uppercase">My Directory</h2>
        <h3 class="section-subheading text-muted">These are my links and categories</h3>
      </div>
    </div>
    <div class="row d-flex justify-content-center">
      <div class="col-lg-8">
        <form id="addMyCategory" name="sentMessage" class="form-horizontal" novalidate="novalidate">
          <div class="form-group">
            <div class="input-group">
              <input type="text" name="title" class="form-control" id="title" placeholder="Add new category" data-validation-required-message="Please enter your category's title."
                value="<?= $mytitle ?>" required>
              <div class="input-group-append">
                <span data-toggle="popover" title="Title requirements" data-content="It can contain only letters, numbers and whitespaces"
                  data-trigger="hover">
                  <i class="input-group-text">i</i>
                </span>
              </div>
            </div>
            <p class="help-block text-danger"></p>
          </div>
          <div class="form-group">
            <div class="input-group">
              <input type="text" name="desc" class="form-control" id="desc" placeholder="Description"
                value="<?= $mydesc ?>">
              <div class="input-group-append">
                <span data-toggle="popover" title="Category description requirements" data-content="Just a couple of words for yourself if it is necessary"
                  data-trigger="hover">
                  <i class="input-group-text">i</i>
                </span>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div id="success"></div>
          <!-- categories start -->
          <div class="categories">
          <div class="catname"></div>
          <div class="catdesc"></div>
          </div>
          <!-- categories end -->
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
              <button id="sendMessageButton" type="submit" name="submit" class="btn btn-primary btn-xl text-uppercase">Add Category</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>