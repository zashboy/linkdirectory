<section id="">
  <div class="container">
  <div class="col-lg-12"><?= $alertmsg ?></div>
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading text-uppercase">Admin Login</h2>
      </div>
    </div>
    <div class="row d-flex justify-content-center">
      <div class="col-lg-6">
                <form class="form-horizontal text-center" role="form" action="adminlogin" method="post">
                        <div class="form-group">
                            <input class="form-control" placeholder="Username" name="name" type="text" autofocus>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Password" name="psw" type="password" value="">
                        </div>
                        <div class="form-check">
                            <label>
                                <input name="remember" class="form-check-input" type="checkbox" value="Remember Me">Remember Me
                            </label>
                        </div>
                        <!-- Change this to a button or input when using this as a form -->
                        <button type="submit" name="submit" class="btn btn-lg btn-success btn-block">Login</button>
                </form>
        </div>
        </div>
      </div>
      </section>