<?php

class Router 
{

    public function __construct($count, $section)
    {
        $this->controller = new Controller;
        $this->count = $count;
        $this->section = $section;

        try {
            switch ($this->count) {
                case 1: 
                    switch ($this->section[0]) {
                        case '':
                        case '/':
                        case 'index.php':
                        case 'home':
                            $this->controller->getPage('home');
                            break;
                        default:
                            if(class_exists($this->section[0] . '_Controller')) {
                                $controllerName = $this->section[0] . '_Controller';
                                $this->controller = new $controllerName;
                                break;
                                //check the request if it is an ajax request
                            } elseif ((substr($this->section[0], 0, 4) == 'ajax') && ('XMLHttpRequest' == ( $_SERVER['HTTP_X_REQUESTED_WITH'] ?? NULL ))) {
                                if(file_exists('ajax/' . $this->section[0])){
                                    require_once ('ajax/' . $this->section[0]);
                                    break;
                                }
                            } else {
                                $this->controller->getPage($this->section[0]);
                                break;
                            }
                        }
                    break;
                case 2:
                    if(is_numeric($this->section[1])){
                        $this->controller->getPage($this->section[0], $this->section[1]);
                        break;
                    } else {
                        $this->controller->getSectionTwo($this->section[1]);
                        break;
                    }
                case 3:
                    if(is_numeric($this->section[2])){
                        $this->controller->getSectionTwo($this->section[1], $this->section[2]);
                        break;
                    } else {
                        $this->controller->getSectionThree($this->section[2]); 
                        break;
                    }
                case 4:
                    if(is_numeric($this->section[3])){
                        $this->controller->getSectionThree($this->section[2], $this->section[3]);
                        break;
                    }

            default:
                throw new CustomException('404 - Page not found', 404);
                break;
            }
        } catch (CustomException $e) {
            return $e->errorMsg();
        } catch (Throwable $t) {
            Log::general($t->getMessage().' | Caught: '.$t->getFile().' | '.$t->getLine());
        }
    }

}//end class
